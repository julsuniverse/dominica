<?php

namespace frontend\controllers;

use Yii;
use src\services\info\ReviewService;
use src\repositories\info\ReviewRepository;
use src\forms\info\ReviewForm;

class ReviewController extends \yii\web\Controller
{
    private $service;
    private $reviews;

    public function __construct($id, $module, ReviewService $service, ReviewRepository $reviews, array $config = [])
    {
        $this->service = $service;
        $this->reviews = $reviews;
        parent::__construct($id, $module, $config);
    }
    public function actionIndex()
    {
        $reviews = $this->reviews->findAllActive();
        $form = new ReviewForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try{
                $this->service->create($form);
                Yii::$app->session->setFlash('success', 'Спасибо, что оставили отзыв. После проверки модератором, он появится на данной страницце.');
            } catch(\DomainException $e){
                Yii::$app->session->setFlash('error', $e->getMessage());
            }

            return $this->refresh();
        }

        return $this->render('index', [
            'reviews' => $reviews,
            'model' => $form,
        ]);
    }

}
