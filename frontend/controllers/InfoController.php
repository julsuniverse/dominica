<?php
namespace frontend\controllers;

use src\repositories\info\InfoRepository;
use yii\web\Controller;


/**
 * Info controller
 */
class InfoController extends Controller
{
    private $infos;

    public function __construct(
        $id,
        $module,
        InfoRepository $infos,
        array $config = [])
    {
        $this->infos = $infos;
        parent::__construct($id, $module, $config);
    }
    /**
     * @inheritdoc
     */

    /**
     * Displays info page.
     *
     * @return mixed
     */
    public function actionInfo($alias)
    {
        $info = $this->infos->getPage($alias);

        return $this->render('info', [
            'info' => $info,
        ]);
    }

}
