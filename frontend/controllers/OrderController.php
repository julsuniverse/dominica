<?php

namespace frontend\controllers;

use src\services\shop\OrderService;
use src\forms\shop\OrderForm;
use Yii;

class OrderController extends \yii\web\Controller
{
    private $service;

    public function __construct(
        $id, 
        $module, 
        OrderService $service,  
        array $config = []
    )
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }
    public function actionIndex()
    {
        $form = new OrderForm();
        if(!$this->service->getCart()['price'])
        {
            return $this->redirect(['/site/index']);
        }
        if ($form->load(Yii::$app->request->post()) && $form->validate()) 
        {
            try{
                $this->service->create($form);
                $form = new OrderForm();
                return $this->redirect(['success']);
            } catch(\DomainException $e){
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('index', [
            'model' => $form,
        ]);
    }
    public function actionSuccess()
    {
        return $this->render('success');
    }

}
