<?php
namespace frontend\controllers;

use src\entities\Slider;
use src\repositories\SettingsRepository;
use Yii;
use yii\web\Controller;
use src\repositories\shop\SectionRepository;
use src\repositories\shop\ProductRepository;

/**
 * Site controller
 */
class SiteController extends Controller
{
    private $sections;
    private $categories;
    private $products;
    private $settings;

    public function __construct(
        $id,
        $module,
        SectionRepository $sections,
        ProductRepository $products,
        SettingsRepository $settings,
        array $config = [])
    {
        $this->sections = $sections;
        $this->products = $products;
        $this->settings = $settings;
        parent::__construct($id, $module, $config);
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index',[
            'sections' => $this->sections->findAllSections(),
            'products' => $this->products->findProdsToMainPage(),
            'settings' => $this->settings->getMain(),
            'slider' => Slider::find()->orderBy('position')->all(),
        ]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


}
