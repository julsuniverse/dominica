<?php
namespace frontend\controllers;

use src\entities\shop\Product;
use src\entities\shop\Sizetable;
use src\forms\shop\CalcSizeForm;
use src\repositories\SettingsRepository;
use Yii;
use yii\web\Controller;
use src\repositories\shop\SectionRepository;
use src\repositories\shop\CategoryRepository;
use src\repositories\shop\ProductRepository;

class ProductController extends Controller
{
    private $sections;
    private $categories;
    private $products;
    private $settings;

    public function __construct(
        $id, 
        $module, 
        CategoryRepository $categories, 
        SectionRepository $sections, 
        ProductRepository $products,
        SettingsRepository $settings,
        array $config = []
    )
    {
        $this->sections = $sections;
        $this->categories = $categories;
        $this->products = $products;
        $this->settings = $settings;
        parent::__construct($id, $module, $config);
    }
    
    public function actionCatalog($section, $category = null, $sizes = null, $sort = null)
    {
        $section = $this->sections->getByAlias($section);
        $category = $category ? $this->categories->getByAlias($section->id, $category) : null;
        $provider = $this->products->findForCatalog($section->id, $category->id, $sizes, $sort);
        return $this->render('catalog', [
            'section' => $section,
            'category' => $category,
            'sizes' => $sizes,
            'provider' => $provider,
            'sort' =>$sort
        ]);
    }
    public function actionSale($section = false)
    {
        $section = $section ? $this->sections->getByAlias($section) : null;
        $provider = $this->products->findSale($section->id);
        return $this->render('sale', [
            'provider' => $provider,
            'sections' => $this->sections->findAllSections(),
            'section' => $section,
            'settings' => $this->settings->getSale(),
        ]);
    }
    public function actionArchive($section = null, $category = null, $sizes = null, $sort = null)
    {
        $section = $section ? $this->sections->getByAlias($section) : null;
        $category = $category ? $this->categories->getByAlias($section->id, $category) : null;
        $provider = $this->products->findForArchive($section->id, $category->id, $sizes, $sort);
        return $this->render('archive', [
            'section' => $section,
            'category' => $category,
            'sizes' => $sizes,
            'provider' => $provider,
            'sort' =>$sort,
            'settings' => $this->settings->getArchive(),
        ]);
    }
    public function actionProduct($alias)
    {
        $product = $this->products->getByAlias($alias);
        $form = new CalcSizeForm();

        $sizesW = Sizetable::getSizesW();
        $sizesM = Sizetable::getSizesM();
        if ($form->load(Yii::$app->request->post()) && $form->validate())
        {
            try
            {
                if($form->chest)
                    $sizeChest = Sizetable::getSizeChest($form->gender, $form->chest);
                if($form->thigh)
                    $sizeThigh = Sizetable::getSizeThigh($form->gender, $form->thigh);
            }
            catch(\DomainException $e)
            {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('product',[
            'product'=>$product,
            'model' => $form,
            'sizeChest' => $sizeChest,
            'sizeThigh' => $sizeThigh,
            'sizesM' => $sizesM,
            'sizesW' => $sizesW,
        ]);
    }
}
