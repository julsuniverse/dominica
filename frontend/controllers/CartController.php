<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Json;
use src\services\cart\CartService;
use src\repositories\shop\ProductRepository;
use yii\filters\VerbFilter;


class CartController extends \yii\web\Controller
{
    private $service;
    private $products;
    
    public function __construct(
        $id, 
        $module, 
        CartService $service, 
        ProductRepository $products,
        $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
        $this->products = $products;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del-from-cart' => ['POST']
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        $cartdata = $this->service->getCart();
        
        $ids = $cartdata['ids'];
        $total = $cartdata['price'];
        $products = $this->service->getProducts();
        
        return $this->render('index',[
            'products'=>$products,
            'ids'=>$ids,
            'total'=>$total,
        ]);
    }

    public function actionAddToCart($id, $size, $amount=1)
    {
        try{
            $product = $this->products->get($id);
            $data = $this->service->addToCart($product, $size, $amount);
            return Json::encode($data);
        } catch (\DomainException $e)
        {
            return $e->getMessage();
        }
    }
    public function actionDelFromCart($id, $size)
    {
        try{
            $product = $this->products->get($id);
            $this->service->delFromCart($product, $size);
            return $this->redirect(Yii::$app->request->referrer);
        } catch (\DomainException $e)
        {
            return $e->getMessage();
        }
    }
    public function actionUnsetCart()
    {
        $this->service->unsetCart();
    }
}
