<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'ru',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl'=>''
        ],
        'user' => [
            'identityClass' => 'src\entities\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                '<_a:about>' => 'site/<_a>',
                'contact' => 'contact/index',
                /*'signup' => 'auth/signup/signup',
                'signup/<_a:[\w-]+>' => 'auth/signup/<_a>',
                '<_a:login|logout>' => 'auth/auth/<_a>',*/
                'product/<alias:[\w\-]+>'=>'product/product',
                'cart'=>'cart/index',
                'cart/<_a:[\w\-]+>' => 'cart/<_a>',
                'order' => 'order/index',
                'order/success'=>'order/success',
                'sale/<section:[\w\-]+>'=>'product/sale',
                'sale'=>'product/sale',
                'catalog/<section:[\w\-]+>/<category:[\w\-]+>'=>'product/catalog',
                'catalog/<section:[\w\-]+>' => 'product/catalog',
                'archive/<section:[\w\-]+>/<category:[\w\-]+>'=>'product/archive',
                'archive/<section:[\w\-]+>' => 'product/archive',
                'archive' => 'product/archive',
                'reviews'=>'review/index',
                'page/<alias:[\w\-]+>' => 'info/info'
                /*'<_c:[\w\-]+>' => '<_c>/index',
                '<_c:[\w\-]+>/<id:\d+>' => '<_c>/view',
                '<_c:[\w\-]+>/<_a:[\w-]+>' => '<_c>/<_a>',
                '<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_c>/<_a>',*/
            ],
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],

    ],
    'params' => $params,
];
