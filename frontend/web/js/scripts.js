/*$(document).ready(function() {
	$('.popup-with-zoom-anim').magnificPopup({
    	type: 'inline',
    	fixedContentPos: false,
    	fixedBgPos: true,
    	overflowY: 'auto',
    	closeBtnInside: true,
    	preloader: false,
    	midClick: true,
    	removalDelay: 300,
    	mainClass: 'my-mfp-zoom-in'
	});
});*/
var error_message = 'Что-то пошло не так. Перезагрузите страницу и повторите попытку';
$(function() {
	$('a.picture').Chocolat();
});
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: "thumbnails"
  });
});
/*$(function() {
    var menu_ul = $('.menu-drop > li > ul'),
    menu_a  = $('.menu-drop > li > a');
    menu_a.click(function(e) {
        e.preventDefault();
        if(!$(this).hasClass('active')) {
            menu_a.removeClass('active');
            menu_ul.filter(':visible').slideUp('normal');
            $(this).addClass('active').next().stop(true,true).slideDown('normal');
        } else {
            $(this).removeClass('active');
            $(this).next().stop(true,true).slideUp('normal');
        }
    });

});*/
$(document).on('click', '.menu-drop > li > a', function(e){
    e.preventDefault();
    var menu_ul = $('.menu-drop > li > ul'),
    menu_a  = $('.menu-drop > li > a');
    if(!$(this).hasClass('active')) {
        menu_a.removeClass('active');
        menu_ul.filter(':visible').slideUp('normal');
        $(this).addClass('active').next().stop(true,true).slideDown('normal');
    } else {
        $(this).removeClass('active');
        $(this).next().stop(true,true).slideUp('normal');
    }
});
$('.value-plus').on('click', function(){
	var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)+1;
	divUpd.text(newVal);
});

$('.value-minus').on('click', function(){
	var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)-1;
	if(newVal>=1) divUpd.text(newVal);
});
function spiner(selector)
{
    $(selector).html('Подождите <i class="fa fa-spinner fa-spin fa-fw"></i>');
    $(selector).attr('disabled', true);
}
function afterSpiner(selector, text)
{
    $(selector).html(text);
    $(selector).attr('disabled', false);
}
function isJson(str) {
    try {
        return JSON.parse(str);
    } catch (e) {
        return str;
    }
}
$(document).on('click', '#jadd', function(e){
    e.preventDefault();
    var id=$(this).attr('data-id');
    var size=$(this).attr('data-size');
    var amount=$('#inputcount').text();
    if(size)
    {
        $.ajax({
            url: "/cart/add-to-cart",
            type: "GET",
            data: {id : id, size : size, amount : amount},
            success: function(data) {
                var data = isJson(data);
                if(data.count !== undefined)
                {
                    $('.simpleCart_total').text(data.price+'грн.');
                    $('.simpleCart_empty').text("Единиц товара: "+data.count);
                    $('#addModal').modal('show');
                }
                else 
                    alert(data);
                    
                $('#jadd').html('<i class="fa fa-cart-arrow-down" aria-hidden="true"></i> Добавить в корзину');
                $('#jadd').attr('disabled','false');
            },
            error: function(response) {
                alert(error_message);
            },
            beforeSend: function()
            {
                spiner('#jadd');
            }
        });
    } 
    else $('#err_block').css('display', 'block');
});
$('.one_size').on('click', function(e)
{
    e.preventDefault();
    $('#err_block').css('display', 'none');
    var size = $(this).text();
    $('.size_val').text("Размер: "+size);
    $("#jadd").attr('data-size', size);
    
});
$(document).on('change', '.cbinpt', function(){
    var href = $(this).attr('data-a');
    $('#sizeidclick').attr('href', href);
    $('#sizeidclick')[0].click();
});

$(document).on('click', '.cats_butt', function(){
    var selector = $('.product-bottom');
    if(selector.css('display')=="none")
        selector.show( "slow" );
    else selector.hide( "slow" );
    
});

$(document).on('click', '#size_table', function(e){
    e.preventDefault();
    $('#modal_sizes').modal('show');

});

$(document).on('change', '#calcsizeform-gender input', function(){
    if ($(this).val()=="m") {
        $('#calcsizeform-thigh').css('display', 'none');
    } else {
        $('#calcsizeform-thigh').css('display', 'block');
    }
})

