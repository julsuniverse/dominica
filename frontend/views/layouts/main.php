<?php

use frontend\widgets\FooterCategoryWidget;
use frontend\widgets\FooterContactWidget;
use frontend\widgets\FooterMenuWidget;
use frontend\widgets\TopMenuWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\widgets\CartWidget;
use frontend\widgets\TopCategoriesWidget;
AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!--header-->
<div class="header">
    <div class="container">
		<div class="head">
			<div class="logo">
				<a href="/"><img src="/frontend/web/img/logo.png" alt="Домініка"/></a>
			</div>
		</div>
	</div>
	<div class="header-top">
		<div class="container">
            <div class="col-sm-12 header-info">
				<ul >
                    <?= TopMenuWidget::widget([]); ?>
                    <li>
                        <div id="google_translate_element"></div><script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({pageLanguage: 'ru', includedLanguages: 'en,uk', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
                            }
                        </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

                    </li>
				</ul>
            </div>
			<div class="clearfix"> </div>
		</div>
	</div>
		
	<div class="container">
		<div class="head-top">	
            <div class="col-sm-8 col-md-offset-2 h_menu4">
				<nav class="navbar nav_bottom" role="navigation">
 
                 <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header nav_2">
                      <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                   </div> 
                   <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                        <ul class="nav navbar-nav nav_1">
                            <!--<li><a class="color <?php if(Url::to()=='/') echo"active"?>" href="<?=Url::to(['site/index']);?>">Главня</a></li>-->
                            <?= TopCategoriesWidget::widget([
                                'active' => $this->params['active_category'] ?? null
                            ]) ?>
                			<li><a class="color3" href="<?=Url::to(['product/sale']);?>">Распродажа</a></li>
                			<li><a class="color4" href="<?=Url::to(['review/index']);?>">Отзывы</a></li>
                        </ul>
                     </div><!-- /.navbar-collapse -->
                
                </nav>
			</div>
			<div class="col-sm-2 search-right">
				<?=CartWidget::widget();?>
				<div class="clearfix"> </div>		
			</div>
			<div class="clearfix"></div>
		</div>	
	</div>	
</div>
<div class="wrapcontent">
    <?= Alert::widget() ?>
    <?= $content ?>
</div>


<!--//footer-->
<div class="footer">
	<div class="footer-middle">
		<div class="container">
			<div class="col-md-3 col-sm-6 footer-middle-in">
				<a href="/"><img src="/frontend/web/img/logo.png"></a>
				<p></p>
			</div>
			
			<div class="col-md-3 col-sm-6 footer-middle-in">
				<h6>Информация</h6>
				<ul class=" in">
                    <?= FooterMenuWidget::widget([]); ?>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-3 col-sm-6 footer-middle-in">
				<h6>Каталог</h6>
				<ul class="tag-in">
                    <?= FooterCategoryWidget::widget([]); ?>
				</ul>
			</div>
			<div class="col-md-3 col-sm-6 footer-middle-in">
				<h6>Контакты</h6>
				<ul class="tag-in">
                    <?= FooterContactWidget::widget([]); ?>
				</ul>
			</div>
			<div class="clearfix"> </div>
            <div class="col-md-12">
                <p class="footer_p">Сайт не несет никакой ответственности за размещенные изображения и логотипы. Все бренды принадлежат их правообладателям.</p>
            </div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<p class="footer-class">&copy; <?=date('Y');?> Интеренет-магазин "Домініка". </p>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!--//footer-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
