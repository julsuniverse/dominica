<?php
/* @var $this yii\web\View */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="container order">
    <div class="row">
        <div class="col-md-12">
            <h1>Оформление заказа</h1>
            <?php $form = ActiveForm::begin(['id' => 'form-order']); ?>
        
                <?= $form->field($model, 'name')->textInput(['placeholder'=>$model->getAttributeLabel('name')])->label('') ?>
                <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+38 (099) 999-99-99',
                    ])->textInput(['placeholder'=>$model->getAttributeLabel('phone')])->label('') ?>
                <?= $form->field($model, 'city')->textInput(['placeholder'=>$model->getAttributeLabel('city')])->label('') ?>
                <?= $form->field($model, 'email')->input('email', ['placeholder'=>$model->getAttributeLabel('email')])->label('') ?>
                <?= $form->field($model, 'poshta')->textArea(['placeholder'=>$model->getAttributeLabel('poshta')])->label('') ?>
                <?= $form->field($model, 'post_number')->textArea(['placeholder'=>$model->getAttributeLabel('post_number')])->label('') ?>
                <?= $form->field($model, 'pay_type')->dropDownList($model->getPayTypes(), ['prompt'=>$model->getAttributeLabel('pay_type')])->label('') ?>
                <?= $form->field($model, 'additional')->textArea(['placeholder'=>$model->getAttributeLabel('additional')])->label('') ?>

        
                <div class="form-group buttonorder">
                    <?= Html::submitButton('Заказать', ['class' => 'hvr-skew-backward', 'name' => 'order-button']) ?>
                </div>
        
            <?php ActiveForm::end(); ?>
        </div>
    
    </div>
</div>
    
