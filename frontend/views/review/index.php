<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use kartik\rating\StarRating;
?>

<div class="container reviews">
    <h1>Отзывы о нас</h1>

    <div class="row">
        <div class="col-md-8">
            <?php foreach($reviews as $review){?>
            <div class="onereview">
                <span class="name"><?=$review->name;?></span>
                <span class="starsblock"><?=StarRating::widget([
                        'name' => '1',
                        'value' => $review->stars,
                        'pluginOptions' => [
                            'displayOnly' => true,
                            'size' => 'xs'
                        ],
                    ]);?>
                </span>
                <div class="clearfix"></div>
                <p><?=$review->text;?></p>
                <span class="date"><?=date('H:i d-m-Y', $review->date);?></span>
                <div class="clearfix"></div>
            </div>
            <?php }?>
        </div>
        <div class="col-lg-4">
            <p>Заполните поля ниже, что бы оставить свой отзыв.</p>
            <?php $form = ActiveForm::begin(['id' => 'reviews-form']); ?>

                <?= $form->field($model, 'name')->textInput(['placeholder'=>$model->getAttributeLabel('name')])->label('') ?>

                <?= $form->field($model, 'text')->textArea(['rows'=>6, 'placeholder'=>$model->getAttributeLabel('text')])->label('') ?>

                <?= $form->field($model, 'stars')->widget(StarRating::classname(), [ 
                        'pluginOptions' => [
                            'min' => 0,
                            'max' => 5,  
                            'size' => 'xs',
                            'step' => 1,
                            'showClear' => false,
                            'showCaption' => false,
                        ]
                    ])->label('');
                ?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-md-6">{image}</div><div class="col-md-6">{input}</div></div>',
                ])->label('') ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'hvr-skew-backward', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>