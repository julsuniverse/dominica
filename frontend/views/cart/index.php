<?php 
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = "Корзина покупок";
?>
<div class="check-out">
    <div class="container">
        <div class="row">
            <h1>Корзина покупок</h1>
        </div>
        <?php if($ids){?>
    	<div class="bs-example4" data-example-id="simple-responsive-table">
            <div class="table-responsive">
                <table class="table-heading">
                    <tr>
                        <th>Фото</th>	
                        <th>Товар</th>	
                        <th>Размер</th>	
                        <th>Цена</th>
                        <th>Количество </th>
                        <th>Стоимость</th>
                        <th>Удалить</th>
                    </tr>
                    <?php $i=0; foreach($products as $prod){?>
                    <tr class="cart-header">
                        <td>
                            <a href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>" class="at-in">
                                <img src="/frontend/web/img/<?=$prod->photo;?>" class="img-responsive" alt="<?=$prod->name;?>"/>
                            </a>
                        </td>
                    	<td>
                        	<div class="sed">
                        		<h5><a href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>"><?=$prod->name;?></a></h5>
                        	</div>
                        </td>
                        <td>
                            <span class="label label-info"><?=$ids[$i]['size'];?></span>
                        </td>
                        <td><?=$ids[$i]['price'];?> грн.</td>
                    	<td><?=$ids[$i]['count'];?></td>
                    	<td class="item_price"><?=$prod->real_price * $ids[$i]['count'];?> грн.</td>
                    	<td>
                            <?= Html::a('<div class="close1"> </div>', ['cart/del-from-cart', 'id' => $prod->id, 'size' => $ids[$i]['size']], [ 'data' => ['confirm' => 'Вы уверены, что желаете удалить товар?','method' => 'post']]) ?>
                        </td>
                        
                    </tr>
                    <?php $i++; }?>
                </table>
        	</div>
    	</div>
        <div class="panel panel-default">
          <div class="panel-heading">Общая стоимость:</div>
          <div class="panel-body">
                <?=$total;?> грн.
          </div>
        </div>
    	<div class="produced">
    	   <a href="<?=Url::toRoute(['order/index']);?>" class="hvr-skew-backward">Сделать заказ</a>
    	</div>
        <?php } else {?>
        <div class="alert alert-danger" role="alert">Вы еще не добавляли товар в корзину</div>
        <?php }?>
    </div>
</div>