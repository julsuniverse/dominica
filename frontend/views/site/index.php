<?php
use yii\helpers\Url;

$this->title = $settings->main_seo_title;

$this->registerMetaTag([
    'name'=>'description',
    'content'=>$settings->main_seo_text
]);
$this->registerMetaTag([
    'name'=>'keywords',
    'content'=>$settings->main_seo_keys
]);
?>
<!--content-->
<div class="content main_page">
    <div class="container-fluid slider_main">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php for($i=0; $i<count($slider); $i++){?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?=$i;?>" class="<?php if($i==0) echo "active"?>"></li>
                <?php }?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php $j = 0; foreach($slider as $slide){ ?>
                    <div class="item <?php if($j==0) echo "active"?>">
                        <a href="<?= $slide->href ;?>">
                            <img src="/frontend/web/img/<?= $slide->photo ;?>" />
                        </a>
                    </div>
                    <?php $j=1;} ?>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div class="main_text">
	    <div class="container">
            <div class="col-md-12">
                <h1><?= $settings->main_h1;?></h1>
                <p><?= $settings->main_text;?></p>
            </div>
            <!-- </div>
    </div>

            <!--<div class="content-top">
            <div class="col-md-6 col-md">
				<div class="col-2">
					<img src="/frontend/web/img/<?= $settings->main_img;?>" class="img-responsive" />
				</div>
			</div>
			<div class="col-md-6 col-md">
				<div class="col-2">
					<h1><?= $settings->main_h1;?></h1>
                    <p><?= $settings->main_text;?></p>
					<a href="<?= Url::toRoute(['info/info', 'alias'=> 'o-nas']);?>" class="buy-now">Подробнее про нас</a>
				</div>
			</div>
            <div class="clearfix"></div>
            <!--
            <?php foreach($sections as $section){?>
			<div class="col-md-6 col-md1">
				<div class="col-3">
					<a href="<?=Url::toRoute(['product/catalog', 'section'=>$section->alias]);?>"><img src="/frontend/web/img/<?=$section->photo;?>" class="img-responsive" alt="<?=$section->name;?>"/>
					<div class="col-pic">
						<p>Одежда</p>
						<label></label>
						<h5><?=$section->name;?></h5>
					</div></a>
				</div>
            </div>
            <?php }?>
			<div class="clearfix"></div>
		</div>
		<!--products-->
            <!-- <div class="container"> -->
    <?php if($products['home']) { ?>
	<div class="content-mid">
		<h3>Домашняя одежда</h3>
		<label class="line"></label>
		<div class="mid-popular">
            <div class="rec_slider">
                <?php foreach($products['home'] as $prod){?>
                    <div class="one_item">
                        <div class="col-md-12 item-grid">
                            <div class="mid-pop">
                                <div class="pro-img">
                                    <img src="/frontend/web/img/<?=$prod->thumb;?>" class="img-responsive" alt="<?=$prod->name;?>">
                                    <div class="zoom-icon ">
                                        <a class="picture" href="/frontend/web/img/<?=$prod->photo;?>" rel="title" class="b-link-stripe b-animate-go  thickbox"><i class="glyphicon glyphicon-search icon "></i></a>
                                        <a href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>"><i class="glyphicon glyphicon-menu-right icon"></i></a>
                                    </div>
                                </div>
                                <div class="mid-1">
                                    <div class="women">
                                        <span><?=$prod->category->name;?></span>
                                        <h6><a href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>"><?=$prod->name;?></a></h6>
                                    </div>
                                    <div class="mid-2">
                                        <p ><?php if($prod->sale){?><label><?=$prod->price;?> грн.</label><?php }?><em class="item_price"><?=$prod->real_price;?> грн.</em></p>
                                        <div class="block">
                                            <div class="starbox small ghosting"> </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="divwithbut">
                                    <a href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div>
		</div>
	</div>
    <?php } ?>
    <?php if($products['rest']) { ?>
    <div class="content-mid">
        <h3>Одежда для отдыха</h3>
        <label class="line"></label>
        <div class="mid-popular">
            <div class="rec_slider">
                <?php foreach($products['rest'] as $prod){?>
                    <div class="one_item">
                        <div class="col-md-12 item-grid">
                            <div class="mid-pop">
                                <div class="pro-img">
                                    <img src="/frontend/web/img/<?=$prod->thumb;?>" class="img-responsive" alt="<?=$prod->name;?>">
                                    <div class="zoom-icon ">
                                        <a class="picture" href="/frontend/web/img/<?=$prod->photo;?>" rel="title" class="b-link-stripe b-animate-go  thickbox"><i class="glyphicon glyphicon-search icon "></i></a>
                                        <a href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>"><i class="glyphicon glyphicon-menu-right icon"></i></a>
                                    </div>
                                </div>
                                <div class="mid-1">
                                    <div class="women">
                                        <span><?=$prod->category->name;?></span>
                                        <h6><a href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>"><?=$prod->name;?></a></h6>
                                    </div>
                                    <div class="mid-2">
                                        <p ><?php if($prod->sale){?><label><?=$prod->price;?> грн.</label><?php }?><em class="item_price"><?=$prod->real_price;?> грн.</em></p>
                                        <div class="block">
                                            <div class="starbox small ghosting"> </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="divwithbut">
                                    <a href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if($products['casual']) { ?>
    <div class="content-mid">
        <h3>Повседневная одежда</h3>
        <label class="line"></label>
        <div class="mid-popular">
            <div class="rec_slider">
                <?php foreach($products['casual'] as $prod){?>
                    <div class="one_item">
                        <div class="col-md-12 item-grid">
                            <div class="mid-pop">
                                <div class="pro-img">
                                    <img src="/frontend/web/img/<?=$prod->thumb;?>" class="img-responsive" alt="<?=$prod->name;?>">
                                    <div class="zoom-icon ">
                                        <a class="picture" href="/frontend/web/img/<?=$prod->photo;?>" rel="title" class="b-link-stripe b-animate-go  thickbox"><i class="glyphicon glyphicon-search icon "></i></a>
                                        <a href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>"><i class="glyphicon glyphicon-menu-right icon"></i></a>
                                    </div>
                                </div>
                                <div class="mid-1">
                                    <div class="women">
                                        <span><?=$prod->category->name;?></span>
                                        <h6><a href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>"><?=$prod->name;?></a></h6>
                                    </div>
                                    <div class="mid-2">
                                        <p ><?php if($prod->sale){?><label><?=$prod->price;?> грн.</label><?php }?><em class="item_price"><?=$prod->real_price;?> грн.</em></p>
                                        <div class="block">
                                            <div class="starbox small ghosting"> </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="divwithbut">
                                    <a href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
    <?php } ?>
	<!--//products-->
        </div> </div>
	
</div>
<!--//content-->
