<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use frontend\widgets\LeftCategoriesWidget;
use frontend\widgets\SizesWidget;
$this->params['active_category'] = $section->id;
$this->title = $settings->archive_seo_title;
$this->registerMetaTag([
    'name'=>'description',
    'content'=>$settings->archive_seo_text,
]);
$this->registerMetaTag([
    'name'=>'keywords',
    'content'=>$settings->archive_seo_keys,
]);
?>
<?php Pjax::begin(['id'=>'catalog_pjax', 'timeout'=>5000, 'scrollTo'=>100]);?>
<div class="product">
    <div class="container">
        <div class="row">
            <h1><?php echo 'Архив '; echo $category->name ? $category->name.' '.$section->name : $section->name;?></h1>
            <button class="btn btn-warning cats_butt">Фильтры <span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></button>
        </div>
    </div>
	<div class="container">
        <div class="col-md-12">
            <div class="dropdown">
              <button class="btn btn-default btn_product dropdown-toggle" type="button" id="dMSort" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <?php if(!$sort) echo "Сортировать";
                    elseif($sort=="date") echo "Сначала новые";
                    elseif($sort=="price") echo "От дешевых к дорогим";
                    elseif($sort=="name") echo "По алфафиту";
                ?>
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu" aria-labelledby="dMSort">
                <?php if($sort!="date"){?>
                <li><a href="<?=Url::toRoute(['section'=>$section->alias, 'category'=>$category->alias, 'sort'=>'date', 'sizes'=>$sizes]);?>">Сначала новые</a></li>
                <?php } 
                if ($sort!="price"){?>
                <li><a href="<?=Url::toRoute(['section'=>$section->alias, 'category'=>$category->alias, 'sort'=>'price', 'sizes'=>$sizes]);?>">От дешевых к дорогим</a></li>
                <?php }
                if($sort!="name"){?>
                <li><a href="<?=Url::toRoute(['section'=>$section->alias, 'category'=>$category->alias, 'sort'=>'name', 'sizes'=>$sizes]);?>">По алфафиту</a></li>
                <?php }?>
              </ul>
            </div>
        </div>
		<div class="col-md-9">
			<div class="mid-popular">
            <?php $products = $provider->getModels();
            if($products){
                foreach($products as $prod){?>
                <div class="col-md-4 item-grid">
        			<div class="mid-pop">
            			<div class="pro-img">
            				<img src="/frontend/web/img/<?=$prod->thumb;?>" class="img-responsive" alt="<?=$prod->name;?>">
            				<div class="zoom-icon ">
            				    <a class="picture" href="/frontend/web/img/<?=$prod->photo;?>" rel="title" class="b-link-stripe b-animate-go  thickbox"><i class="glyphicon glyphicon-search icon "></i></a>
            				    <a data-pjax="0" href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>"><i class="glyphicon glyphicon-menu-right icon"></i></a>
            				</div>
        				</div>
        				<div class="mid-1">
        				    <div class="women">
            					<span><?=$prod->category->name;?></span>
            					<h6><a data-pjax="0" href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>"><?=$prod->name;?></a></h6>
        					</div>
        					<div class="mid-2">
        						<p ><?php if($prod->sale){?><label><?=$prod->price;?> грн.</label><?php }?><em class="item_price"><?=$prod->real_price;?> грн.</em></p>
        						  <div class="block">
        							<div class="starbox small ghosting"> </div>
        						</div>
        						
        						<div class="clearfix"></div>
        					</div>	
        				</div>
                        <div class="divwithbut">
        					<a data-pjax="0" href="<?=Url::toRoute(['product/product', 'alias'=>$prod->alias]);?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Подробнее</a>
                        </div>
        			</div>
        		</div>
                <?php }?>
				<div class="clearfix"></div>
                <?php if($pagination = $provider->getPagination()) {?>
                    <div class="pags">
                        <?=LinkPager::widget([
                            'pagination'=>$pagination,
                            'prevPageLabel'=>'&laquo;',
                            'maxButtonCount'=>4
                        ]);?>
                    </div>
                <?php }?>
                <?php } else {?>
                <div class="alert alert-danger" role="alert">К сожалению, по данному запросу товаров не обнаружено :(</div>
                <?php }?>
			</div>
		</div>
		<div class="col-md-3 product-bottom">
		<!--categories-->
			<div class=" rsidebar span_1_of_left">
				<h4 class="cate">Категории</h4>
				<?= LeftCategoriesWidget::widget([
                    'active_section' => $section->id,
                    'sort' => $sort,
                    'active_category' => $category->id,
                    'sizes' => $sizes
                ]) ?> 
			</div>
            <section  class="sky-form">
				<h4 class="cate">Размер</h4>
				 <div class="row row1 scroll-pane">
					 <div class="col col-4">
                        <?= SizesWidget::widget([
                            'active_section' => $section->alias,
                            'active_category' => $category->alias,
                            'sort' => $sort,
                            'active_sizes' => $sizes
                        ]) ?> 
					 </div>
				 </div>
            </section> 				 				 		
		</div>
		<div class="clearfix"></div>
	</div>	
</div>

<?php Pjax::end();?>