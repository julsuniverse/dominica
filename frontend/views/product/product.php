<?php

use yii\bootstrap\Alert;

$this->title = $product->name;
$this->registerMetaTag([
    'name'=>'description',
    'content'=>$product->seo_desc
]);
$this->registerMetaTag([
    'name'=>'keywords',
    'content'=>$product->seo_keys
]);
?>
<div class="single">
    <div class="container prduct_container">
        <div class="col-md-12">
        	<div class="col-md-5 grid">		
        		<div class="flexslider">
        			  <ul class="slides">
            			    <li data-thumb="/frontend/web/img/<?=$product->thumb;?>">
            			        <div class="thumb-image"> <img src="/frontend/web/img/<?=$product->photo;?>" data-imagezoom="true" class="img-responsive"> </div>
            			    </li>
                            <?php foreach(explode("##", $product->photos) as $photo){
                            if($photo){?>
            			    <li data-thumb="/frontend/web/img/<?=$photo;?>">
            			         <div class="thumb-image"> <img src="/frontend/web/img/<?=$photo;?>" data-imagezoom="true" class="img-responsive"> </div>
            			    </li>
                            <?php }}?> 
        			  </ul>
        		</div>
       	</div>	
        <div class="col-md-7 single-top-in">
            <div class="span_2_of_a1">
                <h1><?=$product->name;?></h1>
    		    <div class="price_single">
                    <?php if(!$product->sale){ ?>
                        <span class="reducedfrom item_price"><?=$product->real_price;?> грн.</span>
                    <?php } else {?>
                        <span class="reducedfrom item_old_price"><?=$product->price;?> грн.</span>
                        <span class="reducedfrom item_price"><?=$product->real_price;?> грн.</span>
                    <?php } ?>
    			     <span class="code"><?=$product->code;?></span>
    			     <div class="clearfix"></div>
    			</div>
                <h4 class="quick">О товаре:</h4>
                <div class="facts">
                    <ul>
                        <li><b>Полотно: </b><?=$product->polotno;?></li>
                        <li><b>Размеры: </b><?=implode(', ', $product->sizes);?></li>
                        <?php if($product->amount) { ?>
                        <li><b>Количество: </b><?=$product->amount;?></li>
                        <?php }
                        elseif($product->in_stock){ ?>
                        <li><b>В наличии <i class="fa fa-check" aria-hidden="true"></i></b></li>
                        <?php } ?>
                    </ul>
                </div>
                <p class="quick_desc"><?=$product->short_desc;?></p>
                <?php if(($product->amount > 1 && !$product->archive) || $product->in_stock == true) {?>
    		    <div class="wish-list">
    			 	<ul>
    			 		<li>
                            <div class="dropdown">
                              <button class="btn btn-default btn_product dropdown-toggle" type="button" id="dropdownSize" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <span class="size_val">Выберите размер</span>
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <?php foreach($product->sizes as $size){ ?>
                                <li class="one_size" ><a href="#"><?= $size;?></a></li>
                                <?php } ?>
                              </ul>
                              <div id="err_block">
                                Пожалуйста, выберите размер!
                              </div>
                            </div>
                        </li>
                    </ul>
                    <div class="compare"><a href="#" id="size_table"><span class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span>Таблица размеров</a></div>
                    <div class="clearfix"></div>
                </div>

    			 <div class="quantity"> 
    				<div class="quantity-select">                           
    					<div class="entry value-minus">&nbsp;</div>
    					<div id="inputcount" class="entry value"><span>1</span></div>
    					<div class="entry value-plus active">&nbsp;</div>
    				</div>
    			</div>
    		    <a href="#" data-id="<?=$product->id;?>" id="jadd" class="add-to item_add hvr-skew-backward">Добавить в корзину</a>
    			<div class="clearfix"> </div>
                <?php } else {?>
                <div class="alert alert-danger" role="alert">К сожалению, товар закончился или находится в архиве. Вы не можете заказать его.</div>
                <?php }?>
            </div>	
        </div>
        <div class="clearfix"> </div>
            <div class="tab-head">
                <nav class="nav-sidebar">
            		<ul class="nav tabs">
                      <li class="active"><a href="#tab1" data-toggle="tab">Полное описание</a></li>
                      <!-- <li class=""><a href="#tab2" data-toggle="tab">Дополнительная информация</a></li> -->
            		</ul>
            	</nav>
            	<div class="tab-content one">
                    <div class="tab-pane active text-style" id="tab1">
                        <div class="facts">
                			  <p ><?=$product->desc;?></p>        
                        </div>
            
                    </div>
                    <!-- <div class="tab-pane text-style" id="tab2">
            			<div class="facts">									
            				<ul>
                                <li><b>Полотно: </b><?=$product->polotno;?></li>
                                <li><b>Размеры: </b><?=implode(', ', $product->sizes);?></li>
                                <li><b>Количество: </b><?=$product->amount;?></li>
                            </ul>
            	        </div>	
                    </div> -->
              </div>
              <div class="clearfix"></div>
            </div>		<!---->	
        </div>
    	<div class="clearfix"> </div>
    </div>
</div>	

<div class="modal fade modal_addcart" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabeAdd">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
        <div class="row">
        <h4 class="modal-title" id="addModalLabeAdd">Товар был успешно добавлен в корзину!</h4>
            <div class="col-sm-6 col-xs-12">
                <button type="button" class="add_cart_active" data-dismiss="modal" aria-label="Close">Продолжить покупки</button>
            </div>
            <div class="col-sm-6 col-xs-12">
                <a href="/cart" type="button" class="add_cart">Перейти в корзину</a>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_sizes" tabindex="-1" role="dialog" aria-labelledby="addModalLabeAdd">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addModalLabeAdd">ТАБЛИЦА РАЗМЕРОВ ТРИКОТАЖНЫХ ИЗДЕЛИЙ  Домініка</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?=$this->render('_sizes', [
                                'model'=>$model,
                                'sizeChest' => $sizeChest,
                                'sizeThigh' => $sizeThigh,
                                'sizesM' => $sizesM,
                                'sizesW' => $sizesW,
                        ]);?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>