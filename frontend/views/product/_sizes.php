<?php

use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


?>


<div class="sizetable_text">
    <?php if(Yii::$app->session->hasFlash('error')) echo Alert::widget() ?>
    <p class="text-muted">Все модели, представленные в нашем магазине, отшиваются на стандартную женскую фигуру при росте 164-170см и стандартную мужскую фигуру при росте 182-188 см .
        Обращаем Ваше внимание, что в таблице приведены мерки фигуры!
    </p>
    <p>
    Покупая одежду в магазине, её следует правильно подбирать в соответствии с размером и Вашей фигурой. 
    </p>
    <p>
        Для трикотажа мерки снимают так же, как и для швейных изделий, только делают поправку на эластичность полотна. 
    </p>
    <p>
        Снимайте мерки прямо поверх нижнего белья, которое вы обычно носите, предварительно повязав по талии сантиметровую ленту.
    </p>
    <p>
        Пожалуйста, введите ваши мерки в предлагаемую  таблицу и узнайте свой размер, соответствующий нашим швейным лекалам.
    </p>
    <div class="row">
        <div class="col-md-5">
            <img class="center-block" src="/frontend/web/img/sizetable.png" />
        </div>
        <div class="col-md-7">
            <p class="sizetable_big">1 - Обхват груди:</p>
            <p> спереди сантиметровая лента проходит по наиболее выступающим точкам груди, сбоку – под подмышечными впадинами.</p>
            <p class="sizetable_big">2 - Обхват талии:</p>
            <p>измеряется строго горизонтально по естественной линии талии.</p>
            <p class="sizetable_big"> 3 - Обхват бедер: </p>
            <p>сантиметровая лента проходит строго горизонтально по наиболее выступающим точкам ягодиц. Обычно на 18-23 см ( в зависимости от роста) ниже линии талии.</p>
        </div>
    </div>
</div>
<div class="sizetable">
    <h3>Определить размер</h3>
    <?php Pjax::begin(['id'=>'sizetablepjax', 'timeout'=>500000]); ?>
        <?php $form = ActiveForm::begin( ['options'=>['data-pjax'=>true, 'class' => '']]); ?>

        <?= $form->field($model, 'chest')->textInput(['maxlength' => true, 'placeholder' => 'Обхват груди (см)'])->label(false) ?>
        <?= $form->field($model, 'thigh')->textInput(['maxlength' => true, 'placeholder' => 'Обхват бедер (см)', 'style' => $model->gender == 'm' ?  'display: none' : 'display: block'])->label(false) ?>
        <?=$form->field($model, 'gender')
        ->radioList([
            'w' => 'Для женщин',
            'm' => 'Для мужчин',
        ])->label(false); ;?>

        <div class="form-group">
            <?= Html::submitButton('Подобрать', ['class' => 'hvr-skew-backward']) ?>
        </div>

        <?php ActiveForm::end(); ?>

        <ul class="list-group">
            <li class="list-group-item">
                <span class="badge">
                    <?= $sizeChest->size_number.'-'.$sizeChest->size_letter;?>
                </span>
                Сорочки, халаты, джемпера:
            </li>
            <li class="list-group-item">
                <span class="badge">
                    <?= $sizeThigh->size_number.'-'.$sizeThigh->size_letter;?>
                </span>
                Брюки, капри:
            </li>
        </ul>
    <?php Pjax::end(); ?>
</div>

<div class="sizetable_table">
    <p class="sizetable_big">Для женщин</p>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Размер</th>
                <?php foreach($sizesW as $size) { ?>
                <th><?= $size->size_number;?></th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <?php foreach ($sizesW as $size){ ?>
                    <th><?= $size->size_letter;?></th>
                    <?php } ?>
                </tr>
                <tr>
                    <td>Обхват груди (см)</td>
                    <?php foreach($sizesW as $size){ ?>
                    <td><?= $size->chest_start;?>-<?= $size->chest_end;?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td>Обхват талии (см)</td>
                    <?php foreach($sizesW as $size){ ?>
                        <td><?= $size->waist_start;?>-<?= $size->waist_end;?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td>Обхват бедер (см)</td>
                    <?php foreach($sizesW as $size){ ?>
                        <td><?= $size->thigh_start;?>-<?= $size->thigh_end;?></td>
                    <?php } ?>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="sizetable_table">
    <p class="sizetable_big">Для мужчин</p>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Размер</th>
                <?php foreach($sizesM as $size) { ?>
                    <th><?= $size->size_number;?></th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <?php foreach ($sizesM as $size){ ?>
                    <th><?= $size->size_letter;?></th>
                <?php } ?>
            </tr>
            <tr>
                <td>Обхват груди (см)</td>
                <?php foreach($sizesM as $size){ ?>
                    <td><?= $size->chest_start;?>-<?= $size->chest_end;?></td>
                <?php } ?>
            </tr>
            <tr>
                <td>Обхват талии (см)</td>
                <?php foreach($sizesM as $size){ ?>
                    <td><?= $size->waist_start;?>-<?= $size->waist_end;?></td>
                <?php } ?>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="sizetable_text">
    <p class="sizetable_big">Обращаем Ваше внимание на то, что длина готового изделия подробно описана в выбранном Вами товаре
        в разделе «Полное описание товара».
    </p>
    <p class="sizetable_extrabig">
        Удачных Вам покупок!
    </p>
</div>
