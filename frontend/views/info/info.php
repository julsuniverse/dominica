<?php

use yii\helpers\Html;

$this->title = $info->name;
$this->registerMetaTag([
    'name'=>'description',
    'content'=>$info->seo_desc
]);
$this->registerMetaTag([
    'name'=>'keywords',
    'content'=>$info->seo_keys
]);

?>
<div class="container infopage">
    <h1><?= Html::encode($this->title) ?></h1>


    <div class="row">
        <div class="col-lg-12">
            <?= $info->text ;?>
        </div>
    </div>

</div>

