<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/style.css',
        'css/style4.css',
        'css/chocolat.css',
        'css/form.css',
        'css/flexslider.css',
        'css/font-awesome.min.css',
        'css/slick.css',
        'css/slick-theme.css',
        //'css/popuo-box.css',
    ];
    public $js = [
        'js/jquery.chocolat.js',
        'js/scripts.js',
        'js/imagezoom.js',
        'js/jquery.flexslider.js',
        'js/slick.min.js',
        'js/main_page_sliders.js',
        //'js/jquery.magnific-popup.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
