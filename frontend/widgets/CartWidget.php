<?php
namespace frontend\widgets;
use src\cart\CartSessionStorage;
use yii\base\Widget;

class CartWidget extends Widget
{
    /** @var Category|null */
    public $cartdata;
    
    public function __construct( CartSessionStorage $cartdata, $config = [])
    {
        parent::__construct($config);

        $this->cartdata = $cartdata;
    }
    public function run()
    {
        return $this->render('cart', [
            'cart' => $this->cartdata->getCart()
        ]);
    }
}