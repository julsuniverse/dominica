<?php
namespace frontend\widgets;
//use src\repositories\shop\CategoryRepository;
use src\repositories\shop\SectionRepository;
use yii\base\Widget;

class TopCategoriesWidget extends Widget
{
    /** @var Category|null */
    public $active;
    //private $categories;
    private $sections;
    
    public function __construct(/*CategoryRepository $categories,*/ SectionRepository $sections, $config = [])
    {
        parent::__construct($config);
        
        //$this->categories = $categories;
        $this->sections = $sections;
    }
    public function run(): string
    {
        return $this->render('topcats', [
            //'categories' => $this->categories->findFirstLevel(),
            'sections' => $this->sections->findSectionsWithCategoriesFirstLvl(),
            'active' => $this->active
        ]);
    }
}