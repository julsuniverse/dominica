<?php
namespace frontend\widgets;
use src\repositories\info\InfoRepository;
use yii\base\Widget;

class FooterMenuWidget extends Widget
{
    private $infos;

    public function __construct(InfoRepository $infos, $config = [])
    {
        $this->infos = $infos;
        parent::__construct($config);

    }
    public function run(): string
    {
        return $this->render('footermenu', [
            'pages' => $this->infos->getFooterMenuPages(),
        ]);
    }
}