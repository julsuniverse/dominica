<?php
namespace frontend\widgets;
use src\repositories\SettingsRepository;
use yii\base\Widget;

class FooterContactWidget extends Widget
{
    private $settings;

    public function __construct(SettingsRepository $settings, $config = [])
    {
        $this->settings = $settings;
        parent::__construct($config);

    }
    public function run(): string
    {
        return $this->render('footercontact', [
            'settings' => $this->settings->getLayout(),
        ]);
    }
}