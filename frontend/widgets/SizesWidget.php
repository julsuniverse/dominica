<?php
namespace frontend\widgets;
use src\repositories\shop\SizeRepository;
use yii\base\Widget;

class SizesWidget extends Widget
{
    public $active_section;
    public $active_category;
    public $active_sizes;
    public $sort;
    
    private $sizes;
    
    public function __construct( SizeRepository $sizes, $config = [])
    {
        parent::__construct($config);

        $this->sizes = $sizes;
    }
    public function run()
    {
        return $this->render('sizes', [
            'sizes' => $this->sizes->findAll(),
            'active_section' => $this->active_section,
            'active_category' => $this->active_category,
            'active_sizes' => $this->active_sizes,
            'sort' => $this->sort
        ]);
    }
}