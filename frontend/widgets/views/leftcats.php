<?php 
    use yii\helpers\Url;
?>
<ul class="menu-drop">
    <?php foreach($sections as $section){?>
	<li class="item">
        <a <?php if($section->id==$active_section){ ?>class="active"<?php }?> href="<?=Url::toRoute(['section'=>$section->alias]);?>"><?=$section->name;?> <span class="caret"></span> </a>
		<ul class="cute" <?php if($section->id==$active_section){ ?>style="display: block;"<?php }?>>
            <li class="subitem1">
                <a href="<?=Url::toRoute(['section' => $section->alias, 'sort'=>$sort, 'sizes'=>$sizes]);?>" <?php if($section->id==$active_section && !$active_category){ ?>class="active"<?php }?>>
                    Все товары раздела
                </a>
            </li>
            <?php foreach($section->categories as $cat){?>
            <li class="subitem1">
                <a href="<?=Url::toRoute(['section'=>$section->alias, 'category'=>$cat->alias, 'sort'=>$sort, 'sizes'=>$sizes]);?>" <?php if($cat->id==$active_category){?> class="active" <?php }?>>
                    <?=str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $cat->depth - 1) . $cat->name;?>
                </a>
            </li>
            <?php }?>
		</ul>
	</li>
    <?php }?>
</ul>