<?php 
    use yii\helpers\Url;
    use src\helpers\SizeHelper;
    
    $arr = explode("--", $active_sizes);
?>
<?php foreach($sizes as $size){?>
<label class="checkbox">
    <input class="cbinpt" 
        data-a="<?=Url::toRoute(['section'=>$active_section, 'category'=>$active_category, 'sort'=>$sort, 'sizes'=>SizeHelper::getSizesStr($size->alias, $arr) ?? null]);?>" 
        type="checkbox" 
        name="checkbox" 
        <?php if(in_array($size->alias, $arr)) {?> checked="" <?php }?>
    />
    <i></i><?=$size->name;?>
</label>
<?php }?>
<a id="sizeidclick" href="#"></a>