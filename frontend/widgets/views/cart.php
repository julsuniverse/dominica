<?php 
    use yii\helpers\Url;
?>
<div class="cart box_1">
	<a href="<?=Url::toRoute(['cart/index']);?>">
	   <h3> <div class="total">
	   <span class="simpleCart_total"><?php echo $cart['price'] ?? 0;?> грн.</span></div>
	   <img src="/frontend/web/img/cart.png" /></h3>
	</a>
	<p><a href="<?=Url::toRoute(['cart/index']);?>" class="simpleCart_empty"><?php echo 'Единиц товаров: '.$cart['count'] ?? 'Корзина пуста';?></a></p>

</div>