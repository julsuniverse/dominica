<?php
    use yii\helpers\Url;
?>
<?php foreach ($sections as $section){?>
<li class="dropdown mega-dropdown">
    <a href="#" class="color1 dropdown-toggle <?php if($section->id==$active) echo "active";?>" data-toggle="dropdown"><?=$section->name;?><span class="caret"></span></a>				
	<div class="dropdown-menu ">
        <div class="menu-top">
			<div class="col1">
				<div class="h_nav">
					<h4><?=$section->name;?></h4>
						<ul>
                            <li>
                                <a href="<?=Url::toRoute(['product/catalog', 'section' => $section->alias]);?>">
                                    <b>Все товары раздела</b>
                                </a>
                            </li>
                            <?php foreach ($section->categories as $category){?>
							<li>
                                <a href="<?=Url::toRoute(['product/catalog', 'section' => $section->alias, 'category' => $category->alias]);?>">
                                    <?=$category->name;?>
                                </a>
                            </li>
                            <?php } ?>
						</ul>	
				</div>							
			</div>
			<div class="col1 col5">
			  <img src="/frontend/web/img/<?=$section->mini_photo;?>" class="img-responsive" />
			</div>
			<div class="clearfix"></div>
		</div>                  
	</div>				
</li>
<?php }?>