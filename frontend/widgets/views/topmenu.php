<?php
?>

<?php use yii\helpers\Url;

foreach ($pages as $page){ ?>
    <li><a href="<?=Url::toRoute(['info/info', 'alias' => $page->alias]);?>"><?= $page->name ;?></a></li>
<?php } ?>
