<?php
namespace frontend\widgets;
use src\repositories\shop\SectionRepository;
use yii\base\Widget;

class LeftCategoriesWidget extends Widget
{
    public $active_section;
    public $active_category;
    public $sort;
    public $sizes;
    private $sections;
    
    public function __construct( SectionRepository $sections, $config = [])
    {
        parent::__construct($config);

        $this->sections = $sections;
    }
    public function run(): string
    {
        return $this->render('leftcats', [
            'sections' => $this->sections->findSectionsWithCategories(),
            'active_section' => $this->active_section,
            'active_category' => $this->active_category,
            'sort' => $this->sort,
            'sizes' => $this->sizes
        ]);
    }
}