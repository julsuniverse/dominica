<?php
namespace frontend\widgets;
use src\repositories\shop\SectionRepository;
use yii\base\Widget;

class FooterCategoryWidget extends Widget
{
    private $sections;

    public function __construct(SectionRepository $sections, $config = [])
    {
        parent::__construct($config);

        $this->sections = $sections;
    }
    public function run(): string
    {
        return $this->render('footercategory', [
            'sections' => $this->sections->findSectionsWithCategoriesFirstLvl(),
        ]);
    }
}