<?php

use yii\db\Migration;

/**
 * Handles adding instock to table `product`.
 */
class m171121_144503_add_instock_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'in_stock', $this->smallInteger(1));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'in_stock');
    }
}
