<?php

use yii\db\Migration;

class m171006_150859_table_product extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
            'section_id' => $this->integer(),
            'category_id' => $this->integer(),
            'subcategory_id' => $this->integer(),
            'photo' => $this->string(),
            'photos' =>$this->string(),
            'polotno' => $this->string(),
            'price' =>$this->double()->notnull(),
            'sale' =>$this->integer(),
            'sizes' => $this->text(),
            'short_desc' => $this->string(),
            'desc' => $this->text(),
            'amount' => $this->integer(),
            'seo_title' => $this->string(),
            'seo_desc' => $this->text(),
            'seo_keys' => $this->text()
        ], $tableOptions);

        $this->createIndex(
            'idx-product-section_id',
            'product',
            'section_id'
        );

        $this->addForeignKey(
            'fk-product-section_id',
            'product',
            'section_id',
            'section',
            'id',
            'SET NULL',
            'CASCADE'
        );
        
        $this->createIndex(
            'idx-product-category_id',
            'product',
            'category_id'
        );

        $this->addForeignKey(
            'fk-product-category_id',
            'product',
            'category_id',
            'category',
            'id',
            'SET NULL',
            'CASCADE'
        );
        
        $this->createIndex(
            'idx-product-subcategory_id',
            'product',
            'subcategory_id'
        );

        $this->addForeignKey(
            'fk-product-subcategory_id',
            'product',
            'subcategory_id',
            'category',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('category');
    }
}
