<?php

use yii\db\Migration;

/**
 * Handles adding mini_photo to table `section`.
 */
class m171007_153458_add_mini_photo_column_to_section_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('section', 'mini_photo', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('section', 'mini_photo');
    }
}
