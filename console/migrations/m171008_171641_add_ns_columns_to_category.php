<?php

use yii\db\Migration;

class m171008_171641_add_ns_columns_to_category extends Migration
{

    public function up()
    {
        $this->addColumn('category', 'lft', $this->integer()->notNull());
        $this->addColumn('category', 'rgt', $this->integer()->notNull());
        $this->addColumn('category', 'depth', $this->integer()->notNull());
        
        $this->createIndex('lft', 'category', ['lft', 'rgt']);
        $this->createIndex('rgt', 'category', ['rgt']);
        
        $this->insert('category', [
             'id' => 1,
             'name' => '',
             'alias' => 'root',
             'section_id' => null,
             'parent_id' => null,
             'desc' => '',
             'seo_title' => '',
             'seo_desc' => '',
             'seo_keys' => '',
             'lft' => 1,
             'rgt' => 2,
             'depth' => 0,
         ]);
    }

    public function down()
    {
        $this->dropColumn('category', 'lft');
        $this->dropColumn('category', 'rgt');
        $this->dropColumn('category', 'dept');
    }
}
