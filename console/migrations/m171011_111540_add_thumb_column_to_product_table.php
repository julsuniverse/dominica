<?php

use yii\db\Migration;

/**
 * Handles adding thumb to table `product`.
 */
class m171011_111540_add_thumb_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'thumb', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'thumb');
    }
}
