<?php

use yii\db\Migration;

/**
 * Handles the creation of table `slider`.
 */
class m171030_110841_create_slider_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('slider', [
            'id' => $this->primaryKey(),
            'photo' => $this->string(),
            'href' => $this->string(),
            'position' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('slider');
    }
}
