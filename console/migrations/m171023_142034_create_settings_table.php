<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m171023_142034_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'main_h1' => $this->string(),
            'main_text' => $this->text(),
            'main_img' => $this->string(),
            'main_seo_title' => $this->string(),
            'main_seo_text' => $this->text(),
            'main_seo_keys' => $this->text(),
            'phone1' => $this->string(),
            'phone2' => $this->string(),
            'contact_email' => $this->string(),
            'admin_email' => $this->string(),
            'sale_seo_title' => $this->string(),
            'sale_seo_text' => $this->text(),
            'sale_seo_keys' => $this->text(),
            'archive_seo_title' => $this->string(),
            'archive_seo_text' => $this->text(),
            'archive_seo_keys' => $this->text(),
        ],  $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings');
    }
}
