<?php

use yii\db\Migration;

class m171020_122824_add_answer_column_toreview extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('review', 'answer', $this->string());
    }

    public function down()
    {
        $this->dropColumn('review', 'answer');
    }
}
