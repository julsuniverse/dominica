<?php

use yii\db\Migration;

class m171006_144434_table_section extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('section', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
            'desc' => $this->text(),
            'seo_title' => $this->string(),
            'seo_desc' => $this->text(),
            'seo_keys' => $this->text()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('section');
    }
}
