<?php

use yii\db\Migration;

class m171014_135928_create_orderitem extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('orderitem', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'amount' => $this->integer()->notNull(),
            'price' => $this->double()->notNull(),
            'size' => $this->string()->notNull(),
        ], $tableOptions);
        
        $this->createIndex(
            'idx-orderitem-order_id',
            'orderitem',
            'order_id'
        );

        $this->addForeignKey(
            'fk-orderitem-order_id',
            'orderitem',
            'order_id',
            'order',
            'id',
            'CASCADE'
        );
        
        $this->createIndex(
            'idx-orderitem-product_id',
            'orderitem',
            'product_id'
        );

        $this->addForeignKey(
            'fk-orderitem-product_id',
            'orderitem',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('orderitem');
    }
}
