<?php

use yii\db\Migration;

/**
 * Handles adding recommended_and_archive to table `product`.
 */
class m171019_104756_add_recommended_and_archive_columns_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'recommended', $this->boolean());
        $this->addColumn('product', 'archive', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'recommended');
        $this->dropColumn('product', 'archive');

    }
}
