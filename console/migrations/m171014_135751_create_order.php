<?php

use yii\db\Migration;

class m171014_135751_create_order extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'status' => $this->integer(1)->notNull(),
            'total' => $this->double()->notNull(),
            'date' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'email' => $this->string(),
            'city' => $this->string()->notNull(),
            'poshta' => $this->text()->notNull(),
            'pay_type' => $this->text()->notNull(),
            'additional' => $this->text(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('order');
    }
    
}
