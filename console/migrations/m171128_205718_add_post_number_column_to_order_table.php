<?php

use yii\db\Migration;

/**
 * Handles adding post_number to table `order`.
 */
class m171128_205718_add_post_number_column_to_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('order', 'post_number', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('order', 'post_number');
    }
}
