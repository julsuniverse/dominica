<?php

use yii\db\Migration;

class m171006_144551_table_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
            'section_id' => $this->integer(),
            'parent_id' => $this->integer(),
            'desc' => $this->text(),
            'seo_title' => $this->string(),
            'seo_desc' => $this->text(),
            'seo_keys' => $this->text()
        ], $tableOptions);
        
        $this->createIndex(
            'idx-category-section_id',
            'category',
            'section_id'
        );

        $this->addForeignKey(
            'fk-category-section_id',
            'category',
            'section_id',
            'section',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('category');
    }
}
