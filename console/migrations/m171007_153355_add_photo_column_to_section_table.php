<?php

use yii\db\Migration;

/**
 * Handles adding photo to table `section`.
 */
class m171007_153355_add_photo_column_to_section_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('section', 'photo', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('section', 'photo');
    }
}
