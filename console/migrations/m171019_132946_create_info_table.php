<?php

use yii\db\Migration;

/**
 * Handles the creation of table `info`.
 */
class m171019_132946_create_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('info', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
            'text' => $this->text(),
            'sort' => $this->integer(),
            'to_top' => $this->boolean(),
            'seo_title' => $this->string(),
            'seo_desc' => $this->text(),
            'seo_keys' => $this->text()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('info');
    }
}
