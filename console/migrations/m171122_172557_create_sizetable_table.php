<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sizetable`.
 */
class m171122_172557_create_sizetable_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('sizetable', [
            'id' => $this->primaryKey(),
            'gender' => $this->string(1),
            'chest_start' => $this->smallInteger(3),
            'chest_end' => $this->smallInteger(3),
            'waist_start' => $this->smallInteger(3),
            'waist_end' => $this->smallInteger(3),
            'thigh_start' => $this->smallInteger(3),
            'thigh_end' => $this->smallInteger(3),
            'size_number' => $this->smallInteger(2),
            'size_letter' => $this->string(10)
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('sizetable');
    }
}
