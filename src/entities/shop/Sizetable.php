<?php

namespace src\entities\shop;

use Yii;

/**
 * This is the model class for table "sizetable".
 *
 * @property integer $id
 * @property string $gender
 * @property integer $chest_start
 * @property integer $chest_end
 * @property integer $waist_start
 * @property integer $waist_end
 * @property integer $thigh_start
 * @property integer $thigh_end
 * @property integer $size_number
 * @property string $size_letter
 */
class Sizetable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sizetable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chest_start', 'chest_end', 'waist_start', 'waist_end', 'thigh_start', 'thigh_end', 'size_number'], 'integer'],
            [['gender'], 'string', 'max' => 1],
            [['size_letter'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gender' => 'Пол',
            'chest_start' => 'Обхват груди мин.',
            'chest_end' => 'Обхват груди макс.',
            'waist_start' => 'Обхват талии мин.',
            'waist_end' => 'Обхват талии макс.',
            'thigh_start' => 'Обхват бедер мин.',
            'thigh_end' => 'Обхват бедер макс.',
            'size_number' => 'Размер',
            'size_letter' => 'Размер',
        ];
    }

    public static function getSizeChest($gender, $chest)
    {
        $size = self::find()
            ->select(['size_number', 'size_letter'])
            ->where(['gender' => $gender])
            ->andWhere(['<=', 'chest_start', $chest])
            ->andwhere(['>', 'chest_end', $chest])
            ->limit(1)
            ->one();

        if(!$size)
            throw new \DomainException('Размер обхвата груди не найден');

        return $size;
    }

    public static function getSizeThigh($gender, $thigh)
    {
        $size = self::find()
            ->select(['size_number', 'size_letter'])
            ->where(['gender' => $gender])
            ->andWhere(['<=', 'thigh_start', $thigh])
            ->andWhere(['>', 'thigh_end', $thigh])
            ->limit(1)
            ->one();

        if(!$size)
            throw new \DomainException('Размер обхвата бедер не найден');

        return $size;
    }

    public static function getSizesW()
    {
        if(!$sizes = self::find()->where(['gender' => 'w'])->all())
            throw new \DomainException('Размеры не найдены');
        return $sizes;
    }

    public static function getSizesM()
    {
        if(!$sizes = self::find()->where(['gender' => 'm'])->all())
            throw new \DomainException('Размеры не найдены');
        return $sizes;
    }
}
