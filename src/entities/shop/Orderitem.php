<?php

namespace src\entities\shop;

use Yii;

/**
 * This is the model class for table "orderitem".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $amount
 * @property double $price
 * @property string $size
 *
 * @property Order $order
 * @property Product $product
 */
class Orderitem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orderitem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'amount', 'price', 'size'], 'required'],
            [['order_id', 'product_id', 'amount'], 'integer'],
            [['price'], 'number'],
            [['size'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'amount' => 'Amount',
            'price' => 'Price',
            'size' => 'Size',
        ];
    }
    public static function create($order_id, $product_id, $amount, $price, $size)
    {
        $orderitem = new self();
        $orderitem->order_id = $order_id;
        $orderitem->product_id = $product_id;
        $orderitem->amount = (int)$amount;
        $orderitem->price = $price;
        $orderitem->size = $size;
        
        return $orderitem;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
