<?php

namespace src\entities\shop;

use src\entities\Seo;
use src\helpers\Aliaser;
use yii\helpers\Json;


/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $alias
 * @property integer $section_id
 * @property integer $category_id
 * @property integer $subcategory_id
 * @property string $photo
 * @property string $photos
 * @property string $polotno
 * @property double $price
 * @property integer $sale
 * @property string $sizes
 * @property string $short_desc
 * @property string $desc
 * @property integer $amount
 * @property string $seo_title
 * @property string $seo_desc
 * @property string $seo_keys
 * @property string $thumb
 * @property boolean recommended
 * @property boolean archive
 * @property integer $in_stock
 *
 * @property Category $category
 * @property Section $section
 * @property Category $subcategory
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }
    public $real_price;
    
    public static function create($name, $code, $price, Seo $seo)
    {
        $product = new self();
        $product->name = $name;
        $product->code = $code;
        $product->alias = Aliaser::alias($name)."-".$product->id;
        $product->price = $price;

        $product->seo_title = $seo->seo_title;
        $product->seo_keys = $seo->seo_keys;
        $product->seo_desc = $seo->seo_desc;

        return $product;
    }

    public function edit($name, $code, $price, Seo $seo)
    {
        $this->name = $name;
        $this->code = $code;
        $this->alias = Aliaser::alias($name)."-". $this->id;
        $this->price = $price;
        $this->seo_title = $seo->seo_title;
        $this->seo_keys = $seo->seo_keys;
        $this->seo_desc = $seo->seo_desc;
    }

    public function setPrice($price, $sale)
    {
        $this->price = $price;
        $this->sale = $sale;
    }

    public function setCategories($section_id, $category_id, $subcategory_id)
    {
        $this->section_id = $section_id;
        $this->category_id = $category_id;
        $this->subcategory_id = $subcategory_id;
    }

    public function setDescription($short_desc, $desc)
    {
        $this->short_desc = $short_desc;
        $this->desc = $desc;
    }

    public function setPhotos($photo, $photos, $thumb)
    {
        $this->photo = $photo;
        $this->photos = $photos;
        $this->thumb = $thumb;
    }

    public function deletePhoto($product, $name)
    {
        $product->photos = str_replace( $name.'##', null, $product->photos);
        $this->setSizes($product->sizes);
    }

    public function setPolotno($polotno)
    {
        $this->polotno = $polotno;
    }

    public function setSizes($sizes)
    {
        $this->sizes = Json::encode($sizes);
    }

    public function setAmount($in_stock, $amount)
    {
        $this->in_stock = $in_stock;
        $this->amount = $amount;
    }

    public function addToArchive($archive)
    {
        $this->archive = $archive;
    }

    public function addToRecommended($recommended, $home, $rest, $casual)
    {
        $this->recommended = $recommended;
        $this->home = $home;
        $this->rest = $rest;
        $this->casual = $casual;
    }

    public function afterFind()
    {
        $this->real_price = $this->sale ? $this->price * (1 - $this->sale / 100) : $this->price;
        $this->sizes = Json::decode($this->sizes);
    }

    /*public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->sizes = Json::encode($this->sizes);

            return true;
        }
        return false;
    }*/

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'alias', 'price'], 'required'],
            [['section_id', 'category_id', 'subcategory_id', 'sale', 'amount', 'recommended', 'archive', 'home', 'rest', 'casual'], 'integer'],
            [['price'], 'number'],
            [['desc', 'seo_desc', 'seo_keys'], 'string'],
            ['sizes', 'each', 'rule' => ['string']],
            [['name', 'code', 'alias', 'polotno', 'thumb', 'short_desc', 'seo_title'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Section::className(), 'targetAttribute' => ['section_id' => 'id']],
            [['subcategory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['subcategory_id' => 'id']],
            ['photo','file','skipOnEmpty' => true, 'extensions' => 'png, jpg, gif', 'checkExtensionByMimeType'=>false],
            ['photos','file','skipOnEmpty' => true, 'extensions' => 'png, jpg, gif', 'maxFiles' => 20, 'checkExtensionByMimeType'=>false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'code' => 'Код',
            'alias' => 'Alias',
            'section_id' => 'Секция',
            'category_id' => 'Категория',
            'subcategory_id' => 'Подкатегория',
            'photo' => 'Основное фото',
            'photos' => 'Фото',
            'polotno' => 'Полотно',
            'price' => 'Цена',
            'sale' => 'Скидка',
            'sizes' => 'Размеры',
            'short_desc' => 'Краткое описание',
            'desc' => 'Описание',
            'amount' => 'Количество в наличии',
            'recommended' => 'Добавить в рекомендуемые',
            'archive' => 'Добавить в архив',
            'home'=>'домашняя одежда Домініка',
            'rest'=>'одежда для отдыха Домініка',
            'casual'=>'повседневная одежда Домініка',
            'seo_title' => 'SEO Заголовок',
            'seo_desc' => 'SEO Описание',
            'seo_keys' => 'SEO Ключевые слова',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'subcategory_id']);
    }

}
