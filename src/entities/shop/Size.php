<?php

namespace src\entities\shop;

/**
 * This is the model class for table "size".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 */
class Size extends \yii\db\ActiveRecord
{
    
    public static function create($name) : self
    {
        $size = new self();
        $size->name = $name;
        $size->alias = \src\helpers\Aliaser::alias($size->name);
        return $size;
    }
    
    public function edit($name)
    {
        $this->name = $name;
        $this->alias = \src\helpers\Aliaser::alias($this->name);
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'size';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['name', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Alias',
        ];
    }
}
