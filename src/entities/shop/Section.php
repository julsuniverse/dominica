<?php

namespace src\entities\shop;

use Yii;
use src\entities\Seo;

/**
 * This is the model class for table "section".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $desc
 * @property string $seo_title
 * @property string $seo_desc
 * @property string $seo_keys
 *
 * @property Category[] $categories
 * @property Product[] $products
 */
class Section extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'section';
    }
    
    public static function create($name, $desc, Seo $seo) : self
    {
        $section = new self();
        $section->name = $name;
        $section->alias = \src\helpers\Aliaser::alias($section->name);
        $section->desc = $desc;
        $section->seo_title = $seo->seo_title;
        $section->seo_desc = $seo->seo_desc;
        $section->seo_keys = $seo->seo_keys;
        
        return $section;
    }
    
    public function edit($name, $desc, Seo $seo)
    {
        $this->name = $name;
        $this->alias = \src\helpers\Aliaser::alias($this->name);
        $this->desc = $desc;
        $this->seo_title = $seo->seo_title;
        $this->seo_desc = $seo->seo_desc;
        $this->seo_keys = $seo->seo_keys;
    }
    public function setPhotos($photo, $mini_photo)
    {
        $this->photo=$photo;
        $this->mini_photo=$mini_photo;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['desc', 'seo_desc', 'seo_keys'], 'string'],
            [['name', 'alias', 'seo_title', 'photo', 'mini_photo'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Alias',
            'desc' => 'Описание',
            'seo_title' => 'Seo заголовок',
            'seo_desc' => 'Seo описнаие',
            'seo_keys' => 'Seo ключевые слова',
            'photo'=>'Фото с главной',
            'mini_photo'=>'Мини фото'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['section_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['section_id' => 'id']);
    }
}
