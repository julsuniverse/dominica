<?php

namespace src\entities\shop;

use Yii;
use src\entities\Seo;
use paulzi\nestedsets\NestedSetsBehavior;
use src\entities\shop\queries\CategoryQuery;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $section_id
 * @property integer $parent_id
 * @property string $desc
 * @property string $seo_title
 * @property string $seo_desc
 * @property string $seo_keys
 *
 * @property Section $section
 * @property Category $prev
 * @property Category $next
 * @mixin NestedSetsBehavior
 * @property Product[] $products
 * @property Product[] $products0
 */
class Category extends \yii\db\ActiveRecord
{
    public static function create($name, $desc, Seo $seo) : self
    {
        $category = new self();
        $category->name = $name;
        $category->alias = \src\helpers\Aliaser::alias($category->name);
        $category->desc = $desc;
        $category->seo_title = $seo->seo_title;
        $category->seo_desc = $seo->seo_desc;
        $category->seo_keys = $seo->seo_keys;
        
        return $category;
    }
    
    public function edit($name, $desc, Seo $seo)
    {
        $this->name = $name;
        $this->alias = \src\helpers\Aliaser::alias($this->name);
        $this->desc = $desc;
        $this->seo_title = $seo->seo_title;
        $this->seo_desc = $seo->seo_desc;
        $this->seo_keys = $seo->seo_keys;
    }
    public function setParts($section_id, $parent_id)
    {
        $this->section_id = $section_id;
        $this->parent_id = $parent_id ? $parent_id : null;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias', 'section_id'], 'required'],
            [['section_id', 'parent_id'], 'integer'],
            [['desc', 'seo_desc', 'seo_keys'], 'string'],
            [['name', 'alias', 'seo_title'], 'string', 'max' => 255],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Section::className(), 'targetAttribute' => ['section_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Alias',
            'section_id' => 'Section ID',
            'parent_id' => 'Parent ID',
            'desc' => 'Desc',
            'seo_title' => 'Seo Title',
            'seo_desc' => 'Seo Desc',
            'seo_keys' => 'Seo Keys',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts0()
    {
        return $this->hasMany(Product::className(), ['subcategory_id' => 'id']);
    }
    public function behaviors(): array
    {
         return [
             NestedSetsBehavior::className()
         ];
    }
     public function transactions(): array
     {
         return [
             self::SCENARIO_DEFAULT => self::OP_ALL,
         ];
     }
    
     public static function find(): CategoryQuery
     {
         return new CategoryQuery(get_called_class());
     }
}
