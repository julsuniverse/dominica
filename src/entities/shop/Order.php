<?php

namespace src\entities\shop;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $status
 * @property double $total
 * @property string $date
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $city
 * @property string $poshta
 * @property string $pay_type
 * @property string $additional
 * @property string $post_number
 *
 * @property Orderitem[] $orderitems
 */
class Order extends \yii\db\ActiveRecord
{
    const STATUS_NEW=1; //новый заказ
    const STATUS_DISPATCH=2; //отправка заказа
    const STATUS_SUCCESS=3; //успешно завершен
    const STATUS_CANCELED=0; //заказ отменен
    
    public static function create($name, $total, $phone, $email, $city, $poshta, $pay_type,  $additional, $post_number)
    {
        $order = new self();
        $order->name = $name;
        $order->total = $total;
        $order->phone = $phone;
        $order->email = $email;
        $order->city = $city;
        $order->poshta = $poshta;
        $order->pay_type = $pay_type;
        $order->post_number = $post_number;
        $order->additional = $additional;
        $order->date = date('U');
        $order->status = self::STATUS_NEW;
        
        return $order;
    }
    public function edit($status, $total, $name, $phone, $city, $poshta, $pay_type, $additional, $post_number)
    {
        $this->status = $status;
        $this->total = $total;
        $this->name = $name;
        $this->phone = $phone;
        $this->email = $email;
        $this->city = $city;
        $this->poshta = $poshta;
        $this->pay_type = $pay_type;
        $this->additional = $additional;
        $this->post_number = $post_number;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'total', 'date', 'name', 'phone', 'city', 'poshta', 'pay_type', 'post_number'], 'required'],
            [['total'], 'number'],
            ['status', 'integer'],
            [['poshta', 'pay_type', 'additional', 'post_number'], 'string'],
            [['date', 'name', 'phone', 'email', 'city'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер заказа',
            'status' => 'Статус',
            'total' => 'Общая сумма (грн.)',
            'date' => 'Дата',
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'city' => 'Город',
            'poshta' => 'Почта',
            'pay_type' => 'Тип оплаты',
            'additional' => 'Дополнительно',
            'post_number' => 'Номер отделения'
        ];
    }
    
    public static function getStatusArray()
    {
        return [
            self::STATUS_NEW=>'Новый заказ',
            self::STATUS_DISPATCH=>'Заказ отправлен',
            self::STATUS_SUCCESS=>'Заказ успешно завершен',
            self::STATUS_CANCELED=>'Заказ отменен'
        ];
    }
    public static function getStatusName($status)
    {
        $statuses= self::getStatusArray();
        return $statuses[$status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderitems()
    {
        return $this->hasMany(Orderitem::className(), ['order_id' => 'id']);
    }
}
