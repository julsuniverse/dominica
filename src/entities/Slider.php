<?php

namespace src\entities;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $photo
 * @property string $href
 * @property integer $position
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position'], 'integer'],
            [['href'], 'string', 'max' => 255],
            ['photo','file','skipOnEmpty' => true, 'extensions' => 'png, jpg, gif', 'checkExtensionByMimeType'=>false]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'photo' => 'Картинка',
            'href' => 'Ссылка',
            'position' => 'Позиция',
        ];
    }
}
