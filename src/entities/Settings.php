<?php

namespace src\entities;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $main_h1
 * @property string $main_text
 * @property string $main_img
 * @property string $main_seo_title
 * @property string $main_seo_text
 * @property string $main_seo_keys
 * @property string $phone1
 * @property string $phone2
 * @property string $contact_email
 * @property string $admin_email
 * @property string $sale_seo_title
 * @property string $sale_seo_text
 * @property string $sale_seo_keys
 * @property string $archive_seo_title
 * @property string $archive_seo_text
 * @property string $archive_seo_keys
 */
class Settings extends \yii\db\ActiveRecord
{
    public static function create() : self
    {
        $settings = new self();
        return $settings;
    }

    public function setMain($main_h1, $main_text, $main_img)
    {
        $this->main_h1 = $main_h1;
        $this->main_text = $main_text;
        $this->main_img = $main_img;
    }

    public function setMainSeo($main_seo_title, $main_seo_text, $main_seo_keys)
    {
        $this->main_seo_title = $main_seo_title;
        $this->main_seo_text = $main_seo_text;
        $this->main_seo_keys = $main_seo_keys;
    }

    public function setContacts($phone1, $phone2, $contact_email, $admin_email)
    {
        $this->phone1 = $phone1;
        $this->phone2 = $phone2;
        $this->contact_email = $contact_email;
        $this->admin_email = $admin_email;
    }

    public function setSaleSeo($sale_seo_title, $sale_seo_text, $sale_seo_keys)
    {
        $this->sale_seo_title = $sale_seo_title;
        $this->sale_seo_text = $sale_seo_text;
        $this->sale_seo_keys = $sale_seo_keys;
    }

    public function setArchiveSeo($archive_seo_title, $archive_seo_text, $archive_seo_keys)
    {
        $this->archive_seo_title = $archive_seo_title;
        $this->archive_seo_text = $archive_seo_text;
        $this->archive_seo_keys = $archive_seo_keys;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main_h1', 'main_img', 'main_seo_title', 'phone1', 'phone2', 'contact_email', 'admin_email', 'sale_seo_title', 'archive_seo_title'], 'string', 'max' => 255],
            [['main_text', 'main_seo_text', 'main_seo_keys', 'sale_seo_text', 'sale_seo_keys', 'archive_seo_text', 'archive_seo_keys'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'main_h1' => 'Заголовок на главной',
            'main_text' => 'Текст на главной',
            'main_img' => 'Картинка на главной',
            'main_seo_title' => 'SEO Заголовок на главной',
            'main_seo_text' => 'SEO Описание на главной',
            'main_seo_keys' => 'SEO Ключевые слова на главной',
            'phone1' => 'Телефон 1',
            'phone2' => 'Телефон 2',
            'contact_email' => 'Контактный E-mail',
            'admin_email' => 'E-mail для уведомлений',
            'sale_seo_title' => 'SEO Заголовок на странице "Распродажа"',
            'sale_seo_text' => 'SEO Описание на странице "Распродажа"',
            'sale_seo_keys' => 'SEO Ключевые слова  на странице "Распродажа"',
            'archive_seo_title' => 'SEO Заголовок в Архиве',
            'archive_seo_text' => 'SEO Описание в Архиве',
            'archive_seo_keys' => 'SEO Ключевые слова в Архиве',
        ];
    }
}
