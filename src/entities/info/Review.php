<?php

namespace src\entities\info;

use Yii;

/**
 * This is the model class for table "review".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property integer $stars
 * @property string $date
 * @property integer $active
 */
class Review extends \yii\db\ActiveRecord
{
    const NOT_ACTIVE = 0;
    const ACTIVE = 1;
    
    public static function create($name, $text, $stars)
    {
        $review = new self();
        $review->name = $name;
        $review->text = $text;
        $review->stars = $stars;
        $review->date = date('U');
        $review->active = self::NOT_ACTIVE;
        
        return $review;
    }
    
    public function setActive()
    {
        $this->active = self::ACTIVE;
    }
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text', 'stars', 'date', 'active'], 'required'],
            [['text','answer'], 'string'],
            [['stars', 'active'], 'integer'],
            [['name', 'date'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'text' => 'Текст отзыва',
            'stars' => 'Количество звезд',
            'date' => 'Дата',
            'active' => 'Активность',
            'answer' => 'Ответ менеджера'
        ];
    }
}
