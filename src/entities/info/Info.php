<?php

namespace src\entities\info;

use src\entities\Seo;
/**
 * This is the model class for table "info".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $text
 * @property integer $sort
 * @property integer $to_top
 * @property string $seo_title
 * @property string $seo_desc
 * @property string $seo_keys
 */
class Info extends \yii\db\ActiveRecord
{

    public static function create($name, $text, $sort, $to_top, Seo $seo) : self
    {
        $info = new self();
        $info->name = $name;
        $info->text = $text;
        $info->sort = $sort;
        $info->to_top = $to_top;

        $info->seo_title = $seo->seo_title;
        $info->seo_keys = $seo->seo_keys;
        $info->seo_desc = $seo->seo_desc;

        $info->alias = \src\helpers\Aliaser::alias($name);

        return $info;
    }

    public function edit($name, $text, $sort, $to_top, Seo $seo)
    {
        $this->name = $name;
        $this->text = $text;
        $this->sort = $sort;
        $this->to_top = $to_top;

        $this->seo_title = $seo->seo_title;
        $this->seo_desc = $seo->seo_desc;
        $this->seo_keys = $seo->seo_keys;

        $this->alias = \src\helpers\Aliaser::alias($name);

    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['text'], 'string'],
            [['sort', 'to_top'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Alias',
            'text' => 'Текст',
            'sort' => 'Позиция',
            'to_top' => 'Показывать в верхнем меню',
            'seo_title' => 'SEO Заголовок',
            'seo_desc' => 'SEO Описание',
            'seo_keys' => 'SEO Ключевые слова',
        ];
    }
}
