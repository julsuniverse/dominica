<?php
namespace src\forms\info;

use src\entities\info\Info;
use src\forms\CompositeForm;
use src\forms\SeoForm;
use src\repositories\info\InfoRepository;

class InfoForm extends CompositeForm
{
    public $id;
    public $name;
    public $text;
    public $sort;
    public $to_top;

    private $infos;

    public function __construct(
        Info $info = null,
        InfoRepository $infos,
        $config = []
    )
    {
        $this->infos = $infos;
        $this->meta = new SeoForm();

        if($info)
        {
            $this->name = $info->name;
            $this->text = $info->text;
            $this->sort = $info->sort;
            $this->to_top = $info->to_top;

            $this->meta->seo_title = $info->seo_title;
            $this->meta->seo_desc = $info->seo_desc;
            $this->meta->seo_keys = $info->seo_keys;
        }

        parent::__construct($config);
    }
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['text'], 'string'],
            [['sort', 'to_top'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'text' => 'Текст',
            'sort' => 'Позиция',
            'to_top' => 'Показывать в верхнем меню',
            'seo_title' => 'SEO Заголовок',
            'seo_desc' => 'SEO Описание',
            'seo_keys' => 'SEO Ключевые слова',
        ];
    }

    protected function internalForms()
    {
        return ['meta'];
    }
}