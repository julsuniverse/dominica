<?php

namespace src\forms\info;

use Yii;

class ReviewForm extends \yii\base\Model
{
    public $name;
    public $text;
    public $stars;
    public $verifyCode;
    
    public function rules()
    {
        return [
            [['name', 'text', 'stars', 'verifyCode'], 'required'],
            [['text'], 'string'],
            [['stars'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Ваше имя',
            'text' => 'Текст отзыва',
            'stars' => 'Количество звезд',
            'verifyCode' => 'Каптча',
        ];
    }
}
