<?php
namespace src\forms;

use src\entities\Settings;
use yii\base\Model;

class SettingsForm extends Model
{
    public $main_h1;
    public $main_text;
    public $main_img;
    public $main_seo_title;
    public $main_seo_text;
    public $main_seo_keys;
    public $phone1;
    public $phone2;
    public $contact_email;
    public $admin_email;
    public $sale_seo_title;
    public $sale_seo_text;
    public $sale_seo_keys;
    public $archive_seo_title;
    public $archive_seo_text;
    public $archive_seo_keys;

    public function __construct(Settings $settings = null, $config = [])
    {
        if ($settings) {
            $this->main_h1 = $settings->main_h1;
            $this->main_text = $settings->main_text;
            $this->main_img = $settings->main_img;
            $this->main_seo_title = $settings->main_seo_title;
            $this->main_seo_text = $settings->main_seo_text;
            $this->main_seo_keys = $settings->main_seo_keys;
            $this->phone1 = $settings->phone1;
            $this->phone2 = $settings->phone2;
            $this->contact_email = $settings->contact_email;
            $this->admin_email = $settings->admin_email;
            $this->sale_seo_title = $settings->sale_seo_title;
            $this->sale_seo_text = $settings->sale_seo_text;
            $this->sale_seo_keys = $settings->sale_seo_keys;
            $this->archive_seo_title = $settings->archive_seo_title;
            $this->archive_seo_text = $settings->archive_seo_text;
            $this->archive_seo_keys = $settings->archive_seo_keys;
        }
        parent::__construct($config);
    }
    public function rules(): array
    {
        return [
            [['main_h1', 'main_img', 'main_seo_title', 'phone1', 'phone2', 'contact_email', 'admin_email', 'sale_seo_title', 'archive_seo_title'], 'string', 'max' => 255],
            [['main_text', 'main_seo_text', 'main_seo_keys', 'sale_seo_text', 'sale_seo_keys', 'archive_seo_text', 'archive_seo_keys'], 'string'],

        ];
    }
    public function attributeLabels()
    {
        return [
            'main_h1' => 'Заголовок на главной',
            'main_text' => 'Текст на главной',
            'main_img' => 'Картинка на главной',
            'main_seo_title' => 'SEO Заголовок на главной',
            'main_seo_text' => 'SEO Описание на главной',
            'main_seo_keys' => 'SEO Ключевые слова на главной',
            'phone1' => 'Телефон 1',
            'phone2' => 'Телефон 2',
            'contact_email' => 'Контактный E-mail',
            'admin_email' => 'E-mail для уведомлений',
            'sale_seo_title' => 'SEO Заголовок на странице "Распродажа"',
            'sale_seo_text' => 'SEO Описание на странице "Распродажа"',
            'sale_seo_keys' => 'SEO Ключевые слова  на странице "Распродажа"',
            'archive_seo_title' => 'SEO Заголовок в Архиве',
            'archive_seo_text' => 'SEO Описание в Архиве',
            'archive_seo_keys' => 'SEO Ключевые слова в Архиве',
        ];
    }
}