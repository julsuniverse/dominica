<?php
namespace src\forms\shop;

use yii\base\Model;

class CalcSizeForm extends Model
{
    public $gender;
    public $chest;
    public $thigh;

    public function rules(): array
    {
        return [
            ['gender', 'required',  'message' => 'Выберите пол'],
            [['chest', 'thigh'], 'integer', 'max' => 200],
            ['gender', 'string'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'chest' => 'Обхват груди',
            'thigh' => 'Обхват бедер',
            'gender' => 'Выберите пол'
        ];
    }
}