<?php

namespace src\forms\shop;

use Yii;

class OrderForm extends \yii\base\Model
{
    public $name;
    public $phone;
    public $city;
    public $email;
    public $poshta;
    public $pay_type;
    public $additional;
    public $post_number;
    
    public function rules()
    {
        return [
            [['name', 'phone', 'city', 'poshta', 'pay_type', 'post_number'], 'required'],
            [['poshta', 'additional', 'post_number'], 'string'],
            [['pay_type', 'name', 'phone', 'email', 'city'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'city' => 'Город',
            'poshta' => 'Почтовая служба (отделение|адрес)',
            'pay_type' => 'Тип оплаты',
            'post_number' => 'Номер отделения',
            'additional' => 'Пожелания',
        ];
    }
    public function getPayTypes()
    {
        return [
            'При получении' => 'При получении',
            'На карту'=>'На карту'
        ];
    }
}
