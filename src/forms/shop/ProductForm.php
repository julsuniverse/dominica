<?php

namespace src\forms\shop;

use src\entities\shop\Category;
use src\entities\shop\Product;
use src\entities\shop\Section;
use src\forms\CompositeForm;
use src\forms\SeoForm;
use src\repositories\shop\CategoryRepository;
use src\repositories\shop\SectionRepository;
use src\repositories\shop\SizeRepository;

class ProductForm extends CompositeForm
{
    public $id;
    public $name;
    public $code;
    public $section_id;
    public $category_id;
    public $subcategory_id;
    public $photo;
    public $photos;
    public $polotno;
    public $price;
    public $sale;
    public $sizes;
    public $short_desc;
    public $desc;
    public $in_stock;
    public $amount;
    public $archive;
    public $recommended;
    public $home;
    public $rest;
    public $casual;

    private $sections;
    private $categories;
    private $sizeRepository;

    /**
     * ProductForm constructor.
     * @param Product|null $product
     * @param SectionRepository $sections
     * @param CategoryRepository $categories
     * @param SizeRepository $sizeRepository
     * @param array $config
     */
    public function __construct(
        Product $product = null,
        SectionRepository $sections,
        CategoryRepository $categories,
        SizeRepository $sizeRepository,
        $config = []
    )
    {
        $this->sections = $sections;
        $this->categories = $categories;
        $this->sizeRepository = $sizeRepository;

        $this->meta = new SeoForm();

        if ($product) {
            $this->name = $product->name;
            $this->code = $product->code;
            $this->section_id = $product->section_id;
            $this->category_id = $product->category_id;
            $this->subcategory_id = $product->subcategory_id;
            $this->photo = $product->photo;
            $this->photos = $product->photos;
            $this->polotno = $product->polotno;
            $this->price = $product->price;
            $this->sale = $product->sale;
            $this->sizes = $product->sizes;
            $this->short_desc = $product->short_desc;
            $this->desc = $product->desc;
            $this->in_stock = $product->in_stock;
            $this->amount = $product->amount;
            $this->recommended = $product->recommended;
            $this->archive = $product->archive;
            $this->home = $product->home;
            $this->rest = $product->rest;
            $this->casual = $product->casual;

            $this->meta->seo_title = $product->seo_title;
            $this->meta->seo_desc = $product->seo_desc;
            $this->meta->seo_keys = $product->seo_keys;

        }
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['name', 'code', 'price'], 'required'],
            [['section_id', 'category_id', 'subcategory_id', 'sale', 'amount', 'recommended', 'archive', 'home', 'rest', 'casual', 'in_stock'], 'integer'],
            [['price'], 'number'],
            [['desc'], 'string'],
            ['sizes', 'each', 'rule' => ['string']],
            [['name', 'code', 'polotno', 'short_desc'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Section::className(), 'targetAttribute' => ['section_id' => 'id']],
            [['subcategory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['subcategory_id' => 'id']],
            ['photo','file','skipOnEmpty' => true, 'extensions' => 'png, jpg, gif', 'checkExtensionByMimeType'=>false],
            ['photos','file','skipOnEmpty' => true, 'extensions' => 'png, jpg, gif', 'maxFiles' => 20, 'checkExtensionByMimeType'=>false],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'code' => 'Код',
            'section_id' => 'Секция',
            'category_id' => 'Категория',
            'subcategory_id' => 'Подкатегория',
            'photo' => 'Основное фото',
            'photos' => 'Фото',
            'polotno' => 'Полотно',
            'price' => 'Цена',
            'sale' => 'Скидка',
            'sizes' => 'Размеры',
            'short_desc' => 'Краткое описание',
            'desc' => 'Описание',
            'in_stock' => 'В наличии',
            'amount' => 'Количество в наличии',
            'recommended' => 'Добавить в рекомендуемые',
            'archive' => 'Добавить в архив',
            'home'=>'домашняя одежда Домініка',
            'rest'=>'одежда для отдыха Домініка',
            'casual'=>'повседневная одежда Домініка',
            'seo_title' => 'SEO Заголовок',
            'seo_desc' => 'SEO Описание',
            'seo_keys' => 'SEO Ключевые слова',
        ];
    }

    public function findSectionsColumn()
    {
        return $this->sections->findSectionsColumn();
    }

    public function findParentsColumn()
    {
        return $this->categories->findParentsColumn($this->section_id);
    }

    public function findSubColumn()
    {
        return $this->categories->findSubCategoryInSection($this->category_id);
    }

    public function getSizes()
    {
        return $this->sizeRepository->getSizes();
    }

    protected function internalForms()
    {
        return ['meta'];
    }
}