<?php
namespace src\forms\shop;

use src\forms\CompositeForm;
use src\forms\SeoForm;
use src\entities\shop\Section;

class SectionForm extends CompositeForm
{
    public $name;
    public $desc;
    public $photo;
    public $mini_photo;
    
    private $_section;
    
    public function __construct(Section $section = null, $config = [])
    {
        $this->meta = new SeoForm();
        
        if ($section) {
            $this->name = $section->name;
            $this->desc = $section->desc;
            $this->meta->seo_title = $section->seo_title;
            $this->meta->seo_desc = $section->seo_desc;
            $this->meta->seo_keys = $section->seo_keys;
            $this->photo = $section->photo;
            $this->mini_photo = $section->mini_photo;
            $this->_section = $section;
        }
        parent::__construct($config);
    }
    public function rules(): array
    {
        return [
            ['name', 'required'],
            [['name', 'photo', 'mini_photo'], 'string', 'max' => 255],
            [ 'desc', 'string'],
            [['photo', 'mini_photo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif', 'checkExtensionByMimeType'=>false],
            [['name'], 'unique', 'targetClass' => Section::class, 'filter' => $this->_section ? ['<>', 'id', $this->_section->id] : null]
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Alias',
            'desc' => 'Описание',
            'photo'=>'Фото с главной',
            'mini_photo'=>'Мини фото'
        ];
    }
    protected function internalForms()
    {
        return ['meta'];
    }
}