<?php
namespace src\forms\shop;

use yii\base\Model;
use src\entities\shop\Size;

class SizeForm extends Model
{
    public $name;
    
    private $_size;
    
    public function __construct(Size $size = null, $config = [])
    {        
        if ($size) {
            $this->name = $size->name;
            $this->_size = $size;
        }
        parent::__construct($config);
    }
    public function rules(): array
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 255],
        ];
    }
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
        ];
    }
}