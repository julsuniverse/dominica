<?php

namespace src\forms\shop;

use Yii;
use src\forms\CompositeForm;
use src\forms\SeoForm;
use src\entities\shop\Category;
use src\entities\shop\Section;
use src\repositories\shop\SectionRepository;
use src\repositories\shop\CategoryRepository;

class CategoryForm extends CompositeForm
{
    public $name;
    public $section_id;
    public $parent_id;
    public $desc;
    
    private $_category;
    private $categories;
    private $sections;
    
    public function __construct(
        Category $category = null, 
        CategoryRepository $categories,
        SectionRepository $sections,
        $config = []
    )
    {
        $this->meta = new SeoForm();
        $this->categories = $categories;
        $this->sections = $sections;
        
        if ($category) {
            $this->name = $category->name;
            $this->desc = $category->desc;
            $this->section_id = $category->section_id;
            $this->parent_id = $category->parent_id ? $category->parent_id : null;
            $this->meta->seo_title = $category->seo_title;
            $this->meta->seo_desc = $category->seo_desc;
            $this->meta->seo_keys = $category->seo_keys;
            
            $this->_category = $category;
        }
        parent::__construct($config);
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'section_id'], 'required'],
            [['section_id', 'parent_id'], 'integer'],
            [['desc'], 'string'],
            ['name', 'string', 'max' => 255],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Section::className(), 'targetAttribute' => ['section_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'section_id' => 'Секция',
            'parent_id' => 'Родительская категория (если есть)',
            'desc' => 'Описание',
        ];
    }
    public function findSectionsColumn()
    {
        return $this->sections->findSectionsColumn();
    }

    public function findParentsColumn()
    {
        return $this->categories->findParentsColumn($this->section_id);
    }
    protected function internalForms()
    {
        return ['meta'];
    }
}
