<?php

namespace src\forms\shop;

use Yii;
use src\entities\shop\Order;

class OrderEditForm extends \yii\base\Model
{
    public $status;
    public $total;
    
    public $name;
    public $phone;
    public $city;
    public $poshta;
    public $pay_type;
    public $additional;
    
    public function __construct(Order $order = null, $config = [])
    {        
        $this->status = $order->status;
        $this->total = $order->total;
        $this->name = $order->name;
        $this->phone = $order->phone;
        $this->city = $order->city;
        $this->poshta = $order->poshta;
        $this->pay_type = $order->pay_type;
        $this->additional = $order->additional;
        
        parent::__construct($config);
    }
    
    public function rules()
    {
        return [
            [['status',], 'required'],
            [['poshta', 'additional'], 'string'],
            ['status', 'integer'],
            ['total', 'number'],
            [['pay_type', 'name', 'phone', 'city'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status' => 'Статус',
            'total' => 'Общая сумма (грн.)',
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'city' => 'Город',
            'poshta' => 'Почтовая служба (отделение|адрес)',
            'pay_type' => 'Тип оплаты',
            'additional' => 'Пожелания',
        ];
    }
}
