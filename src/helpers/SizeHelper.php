<?php

namespace src\helpers;

class SizeHelper
{
    
    public static function getSizesStr($alias, $arr)
    {
        $flag = true;
        $pos = array_search($alias, $arr);  
        if($pos)
        {
            unset($arr[$pos]); 
            $flag = false;
        }       
        if($flag)
            $arr[] = $alias;
        $newsizes = implode('--', $arr);
        return $newsizes ?? null;
    }
    public static function getRegExp($sizes)
    {
        $sizes = explode('--', $sizes);
        $exp = '"'.$sizes[1].'"';
        for($i=2; $i<count($sizes); $i++)
        {
            $exp .= '|"'.$sizes[$i].'"';
        }
        return $exp;
    }
}