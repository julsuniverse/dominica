<?php
namespace src\services\info;

use src\entities\info\Info;
use src\entities\Seo;
use src\forms\info\InfoForm;
use src\repositories\info\InfoRepository;

class InfoService
{

    private $infos;

    public function __construct(InfoRepository $infos)
    {
        $this->infos = $infos;
    }
    public function create(InfoForm $form): Info
    {
        $info = Info::create(
            $form->name,
            $form->text,
            $form->sort,
            $form->to_top,
            new Seo(
                $form->meta->seo_title,
                $form->meta->seo_desc,
                $form->meta->seo_keys
            )
        );
        return $this->infos->save($info);
    }

    public function edit(InfoForm $form, $id): Info
    {
        $info = $this->infos->get($id);
        $info->edit(
            $form->name,
            $form->text,
            $form->sort,
            $form->to_top,
            new Seo(
                $form->meta->seo_title,
                $form->meta->seo_desc,
                $form->meta->seo_keys
            )
        );
        return $this->infos->save($info);
    }
    public function remove($id)
    {
        $size = $this->infos->get($id);
        $this->infos->remove($size);
    }
}