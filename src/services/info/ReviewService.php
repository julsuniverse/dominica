<?php
namespace src\services\info;
use src\entities\info\Review;
use src\forms\info\ReviewForm;
use yii\mail\MailerInterface;
use src\repositories\info\ReviewRepository;

class ReviewService
{

    private $reviews;
    private $mailer;

    public function __construct(ReviewRepository $reviews, MailerInterface $mailer)
    {
        $this->reviews = $reviews;
        $this->mailer = $mailer;
    }
    public function create(ReviewForm $form)
    {
        $review = Review::create(
            $form->name,
            $form->text,
            $form->stars
        );
        $review = $this->reviews->save($review);
        $this->sendMails();
        
        return $review;
    }
    private function sendMails()
    {
        $this->mailer->compose()
            ->setTo('siegigor@gmail.com')
            ->setSubject('Новый отзыв в интернет-магазине "Доминика"')
            ->setHtmlBody("<h3>Новый отзыв в интернет-магазине \"Доминика\"</h3><p>Детали: <a href='".\Yii::$app->urlManager->createAbsoluteUrl('')."yii-admin/review'>Отзывы</a></p>")
            ->send();
    }
    public function setActive($id)
    {
        $review = $this->reviews->get($id);
        $review->setActive();
        $this->reviews->save($review);
    }
    public function remove($id)
    {
        $review = $this->reviews->get($id);
        $this->reviews->remove($review);
    }
}