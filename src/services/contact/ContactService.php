<?php

namespace src\services\contact;

use src\forms\ContactForm;
use src\repositories\SettingsRepository;
use yii\mail\MailerInterface;

class ContactService
{
    private $mailer;
    private  $adminEmail;

    public function __construct(MailerInterface $mailer, SettingsRepository $settings)
    {
        $this->adminEmail = explode(", ", $settings->getAdminEmail());;
        $this->mailer = $mailer;
    }

    public function sendEmail(ContactForm $form)
    {
        $sent =  $this->mailer->compose()
            ->setTo($this->adminEmail)
            ->setSubject($form->subject)
            ->setTextBody($form->body)
            ->send();

        if(!$sent)
            throw new \DomainException('Sending error');
    }
}