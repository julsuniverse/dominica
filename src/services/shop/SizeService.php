<?php
namespace src\services\shop;
use src\entities\shop\Size;
use src\forms\shop\SizeForm;
use src\repositories\shop\SizeRepository;

class SizeService
{
    private $sizes;
    
    public function __construct(SizeRepository $sizes)
    {
        $this->sizes = $sizes;
    }
    public function create(SizeForm $form): Size
    {
        $size = Size::create($form->name);
        return $this->sizes->save($size);
    }
    public function edit(SizeForm $form, $id): Size
    {
        $size = $this->sizes->get($id);
        $size->edit($form->name);
        return $this->sizes->save($size);
    }
    public function remove($id)
    {
        $size = $this->sizes->get($id);
        $this->sizes->remove($size);
    }
}