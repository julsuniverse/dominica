<?php
namespace src\services\shop;
use src\entities\shop\Product;
use src\forms\shop\ProductForm;
use src\repositories\shop\ProductRepository;
use src\services\Imager;
use src\entities\Seo;
use Yii;
use yii\imagine\Image;

class ProductService
{
    private $products;
    private $imager;

    public function __construct(ProductRepository $products, Imager $imager)
    {
        $this->products = $products;
        $this->imager = $imager;
    }

    public function create(ProductForm $form): Product
    {
        $product = Product::create(
            $form->name,
            $form->code,
            $form->price,

            new Seo(
                $form->meta->seo_title,
                $form->meta->seo_desc,
                $form->meta->seo_keys
            )
        );
        $product->setPolotno($form->polotno);
        $product->setSizes($form->sizes);
        $product->setAmount($form->in_stock, $form->amount);
        $product->setPrice($form->price, $form->sale);
        $product->setCategories($form->section_id, $form->category_id, $form->subcategory_id);
        $product->setDescription($form->short_desc, $form->desc);
        $product->addToArchive($form->archive);
        $product->addToRecommended($form->recommended, $form->home, $form->rest, $form->casual);
        $photo = $this->imager->savePhoto('photo', $product->photo, $form);
        $product->setPhotos(
            $photo,
            $this->imager->savePhotos('photos', $product->photos, $form),
            $this->makeThumb($photo)
        );

        $prod = $this->products->save($product);
        $prod->alias = $prod->alias.$prod->id;
        return $this->products->save($prod);
    }

    public function edit(ProductForm $form, $id): Product
    {
        $product = $this->products->get($id);
        $product->edit(
            $form->name,
            $form->code,
            $form->price,

            new Seo(
                $form->meta->seo_title,
                $form->meta->seo_desc,
                $form->meta->seo_keys
            )
        );

        $product->setPolotno($form->polotno);
        $product->setSizes($form->sizes);
        $product->setAmount($form->in_stock, $form->amount);
        $product->setPrice($form->price, $form->sale);
        $product->setCategories($form->section_id, $form->category_id, $form->subcategory_id);
        $product->setDescription($form->short_desc, $form->desc);
        $product->addToArchive($form->archive);
        $product->addToRecommended($form->recommended, $form->home, $form->rest, $form->casual);
        $photo = $this->imager->savePhoto('photo', $product->photo, $form);
        $product->setPhotos(
            $photo,
            $this->imager->savePhotos('photos', $product->photos, $form),
            $this->makeThumb($photo)
        );
        return $this->products->save($product);
    }

    public function remove($id)
    {
        $section = $this->products->get($id);
        $this->products->remove($section);
    }

    public function deletePhoto($name, $product_id)
    {
        $product = $this->products->get($product_id);
        $product->deletePhoto($product, $name);
        //$product->setSizes($sizes);
        $product->save();

        return $this->products->save($product);
    }

    public function makeThumb($photo)
    {
        if(!$photo)
            return "";
        else 
        {
            $path = Yii::getAlias('@frontend').'/web/img/';
            Image::thumbnail($path.$photo, 233, 323)
                ->save($path.'thumb-'.$photo, ['quality' => 100]);
            return 'thumb-'.$photo;  
        }
        
    }
}