<?php
namespace src\services\shop;
use src\entities\shop\Category;
use src\forms\shop\CategoryForm;
use src\repositories\shop\CategoryRepository;
use src\entities\Seo;

class CategoryService
{
    private $categories;
    
    public function __construct(CategoryRepository $categories)
    {
        $this->categories = $categories;
    }
    public function create(CategoryForm $form): Category
    {
        $category = Category::create(
            $form->name, 
            $form->desc, 
            new Seo(
                $form->meta->seo_title, 
                $form->meta->seo_desc, 
                $form->meta->seo_keys
            )
        );
        $category->setParts($form->section_id, $form->parent_id);
        
        $parent = $this->categories->get($form->parent_id);
        $category->appendTo($parent);
        
        return $this->categories->save($category);
    }
    public function edit(CategoryForm $form, $id): Category
    {
        $category = $this->categories->get($id);
        $this->assertIsNotRoot($category);
        $category->edit(
            $form->name, 
            $form->desc, 
            new Seo(
                $form->meta->seo_title, 
                $form->meta->seo_desc, 
                $form->meta->seo_keys
            )
        );
        $category->setParts($form->section_id, $form->parent_id);
        
        if ($form->parent_id !== $category->parent_id) {
             $parent = $this->categories->get($form->parent_id);
             $category->appendTo($parent);
         }
 
        return $this->categories->save($category);
    }
    public function remove($id)
    {
        $category = $this->categories->get($id);
        $this->assertIsNotRoot($category);
        $this->categories->remove($category);
    }
    public function moveUp($id): void
    {
        $category = $this->categories->get($id);
        $this->assertIsNotRoot($category);
        if ($prev = $category->prev) {
            $category->insertBefore($prev);
        }
        $this->categories->save($category);
    }

    public function moveDown($id): void
    {
        $category = $this->categories->get($id);
        $this->assertIsNotRoot($category);
        if ($next = $category->next) {
            $category->insertAfter($next);
        }
        $this->categories->save($category);
    }
    
    private function assertIsNotRoot(Category $category): void
    {
        if ($category->isRoot()) {
             throw new \DomainException('Unable to manage the root category.');
         }
    }
}