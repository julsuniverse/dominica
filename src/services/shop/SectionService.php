<?php
namespace src\services\shop;
use src\entities\shop\Section;
use yii\web\UploadedFile;
use src\forms\shop\SectionForm;
use src\repositories\shop\SectionRepository;
use src\services\Imager;
use src\entities\Seo;

class SectionService
{
    private $sections;
    private $imager;
    
    public function __construct(SectionRepository $sections, Imager $imager)
    {
        $this->sections = $sections;
        $this->imager=$imager;
    }
    public function create(SectionForm $form): Section
    {
        $section = Section::create(
            $form->name, 
            $form->desc, 
            new Seo(
                $form->meta->seo_title, 
                $form->meta->seo_desc, 
                $form->meta->seo_keys
            )
        );
        $section->setPhotos(
            $this->imager->savePhoto('photo', $section->photo, $form), 
            $this->imager->savePhoto('mini_photo', $section->mini_photo, $form)
        );
        return $this->sections->save($section);
    }
    public function edit(SectionForm $form, $id): Section
    {
        $section = $this->sections->get($id);
        $section->edit(
            $form->name, 
            $form->desc, 
            new Seo(
                $form->meta->seo_title, 
                $form->meta->seo_desc, 
                $form->meta->seo_keys
            )
        );
        $section->setPhotos(
            $this->imager->savePhoto('photo', $section->photo, $form), 
            $this->imager->savePhoto('mini_photo', $section->mini_photo, $form)
        );
        
        return $this->sections->save($section);
    }
    public function remove($id)
    {
        $section = $this->sections->get($id);
        $this->sections->remove($section);
    }
}