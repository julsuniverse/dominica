<?php
namespace src\services\shop;
use src\entities\shop\Order;
use src\entities\shop\Orderitem;
use src\forms\shop\OrderForm;
use src\repositories\SettingsRepository;
use yii\mail\MailerInterface;
use src\forms\shop\OrderEditForm;
use src\repositories\shop\OrderRepository;
use src\repositories\shop\OrderitemRepository;
use src\cart\Cart;

class OrderService
{
    private $orders;
    private $cart;
    private $orderitems;
    private $mailer;
    private $settings;

    public function __construct(OrderRepository $orders, Cart $cart, OrderitemRepository $orderitems, MailerInterface $mailer, SettingsRepository $settings)
    {
        $this->orders = $orders;
        $this->cart = $cart;
        $this->orderitems = $orderitems;
        $this->mailer = $mailer;
        $this->settings = $settings;
    }
    public function create(OrderForm $form)
    {
        $cart = $this->cart->getCart();
        $order = Order::create($form->name, $cart['price'], $form->phone, $form->email, $form->city, $form->poshta, $form->pay_type, $form->additional, $form->post_number);
        $cart = $cart['ids'];
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $order = $this->orders->save($order);
            foreach($cart as $item)
            {
                $orderItem = Orderitem::create($order->id, $item['id'], $item['count'], $item['price'], $item['size']);
                $this->orderitems->save($orderItem);
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw new \DomainException('Ошибка сохранения заказа. Повторите попытку.');
        }
        $this->sendMails($order->id);
        $this->cart->unsetCart();
    }
    public function getCart()
    {
        return $this->cart->getCart();
    }
    private function sendMails($id)
    {
        $emails = explode(", ", $this->settings->getAdminEmail());
        $sent =  $this->mailer->compose()
            ->setTo($emails)
            ->setSubject('Новый заказ в интернет-магазине "Доминика"')
            ->setHtmlBody("<h3>Новый заказ в интернет-магазине \"Доминика\"</h3><p>Детали: <a href='".\Yii::$app->urlManager->createAbsoluteUrl('')."yii-admin/order/$id'>Заказ №$id</a></p>")
            ->send();

        if(!$sent)
            throw new \DomainException('Ошибка отправки');
    }
    public function edit(OrderEditForm $form, $id)
    {
        $order = $this->orders->get($id);
        $order->edit($form->status, $form->total, $form->name, $form->phone, $form->city, $form->poshta, $form->pay_type, $form->additional, $form->post_number);
        return $this->orders->save($order);
    }

    public function remove($id)
    {
        $order = $this->orders->get($id);
        $this->orders->remove($order);
    }
}