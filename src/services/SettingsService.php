<?php
namespace src\services;
use src\entities\Settings;
use src\forms\SettingsForm;
use src\repositories\SettingsRepository;

class SettingsService
{
    private $settings;
    private $imager;

    public function __construct(SettingsRepository $settings, Imager $imager)
    {
        $this->settings = $settings;
        $this->imager = $imager;
    }
    public function create(SettingsForm $form): Settings
    {
        $settings = Settings::create();
        $settings = $this->setChanges($form, $settings);
        return $this->settings->save($settings);
    }
    public function edit(SettingsForm $form, $id): Settings
    {
        $settings = $this->settings->get($id);
        $settings = $this->setChanges($form, $settings);
        return $this->settings->save($settings);
    }
    public function remove($id)
    {
        $settings = $this->settings->get($id);
        $this->settings->remove($settings);
    }

    private function setChanges($form, $settings)
    {
        $settings->setMain($form->main_h1, $form->main_text, $this->imager->savePhoto('main_img', $settings->main_img, $form));
        $settings->setMainSeo($form->main_seo_title, $form->main_seo_text, $form->main_seo_keys);
        $settings->setContacts($form->phone1, $form->phone2, $form->contact_email, $form->admin_email);
        $settings->setSaleSeo($form->sale_seo_title, $form->sale_seo_text, $form->sale_seo_keys);
        $settings->setArchiveSeo($form->archive_seo_title, $form->archive_seo_text, $form->archive_seo_keys);

        return $settings;
    }
}