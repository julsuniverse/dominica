<?php

namespace src\services\cart;

use src\cart\Cart;
use src\entities\shop\Product;
use src\repositories\shop\ProductRepository;

class CartService
{
    private $cart;
    private $products;
    
    public function __construct(Cart $cart, ProductRepository $products)
    {
        $this->cart = $cart;
        $this->products = $products;
    }

    public function addToCart(Product $product, $size, $amount)
    {
        if(strpos($product->sizes, '"'.$size.'"')===false){
            throw new \DomainException('Не верно задан размер');
        }
        if($product->amount < $amount){
            throw new \DomainException('На складе только '.$product->amount.' единиц товара');
        }
        
        return $this->cart->addToCart($product, $size, $amount);
        
    }
    public function delFromCart(Product $product, $size)
    {
        $this->cart->delFromCart($product, $size);
    }
    public function unsetCart()
    {
        $this->cart->unsetCart();
    }
    public function getProducts()
    {
        $ids = $this->getCart()['ids'];
        $arr_ids=[];
        for($i=0; $i<count($ids); $i++)
            $arr_ids[]=$ids[$i]['id'];
        $prods=[];
        for($i=0; $i<count($arr_ids);$i++)
            $prods[] = $this->products->get($arr_ids[$i]); 
        return $prods;
    }
    public function getCart()
    {
        return $this->cart->getCart();
    }
}