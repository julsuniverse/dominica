<?php
namespace src\cart;

use yii\web\Session;
use src\cart\CartStorageInterface;
    /*--Format session ids
    [
        [
            'id'=>2,
            'size'=>'33'
            'count'=>3,
            'price'=>500
        ],
        [
            'id'=>1,
            'size'=>'35',
            'count'=>1,
            'price'=>400
        ],
        [
            'id'=>1,
            'size'=>'33',
            'count'=>5,
            'price'=>5000
        ],
    ]
    */
class CartSessionStorage implements CartStorageInterface
{
    public $ids;
    public $count;
    public $price;
    
    private $session;
    
    public function __construct(Session $session)
    {
        $this->session = $session;
        $this->ids = $this->session->get('ids');        
        $this->count = $this->session->get('count');
        $this->price = $this->session->get('price');
    }
    public function setToCart($ids, $count, $price)
    {
        $this->ids = $ids;
        $this->count = $count;
        $this->price = $price;
        
        $this->session->set('ids', $ids);
        $this->session->set('count', $count);
        $this->session->set('price', $price);
        return [
            'count' => $this->count,
            'price' => $this->price
        ];
    }
    public function unsetCart()
    {
        $this->ids = null;
        $this->count = null;
        $this->price = null;
        
        $this->session->remove('ids');
        $this->session->remove('count');
        $this->session->remove('price');
    }
    public function getCart()
    {
        return [
            'ids' => $this->ids,
            'count' => $this->count,
            'price' => $this->price
        ];
    }
    
}