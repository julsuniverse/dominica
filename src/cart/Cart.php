<?php
    
    namespace src\cart;  
    
    use src\entities\shop\Product;
    use yii\helpers\Html;
    use src\cart\CartStorageInterface;
    use Yii;
    
    
    class Cart 
    {   
        private $cart;
        
        public function __construct(CartStorageInterface $cart)
        {
            $this->cart = $cart;
        }
        public function addToCart(Product $product, $size, $count=1)
        {
            $ids = $this->cart->ids;
            $id = $product->id;
            $oldid=$this->inids(['id'=>$id, 'size'=>$size]);
            if($oldid!==false)
                $ids[$oldid]['count']+=$count;
            else
                $ids[]=['id'=>$id,'size'=>$size, 'count'=>$count, 'price'=>$product->real_price];
            
            $res = $this->cart->setToCart($ids, $this->cart->count + $count, $this->cart->price+$product->real_price*$count);
            return $res;
        }

        public function delFromCart(Product $product, $size)
        {   
            $ids=$this->cart->ids;
            $count_el=0;
            $oldid=$this->inids(['id'=>$product->id, 'size'=>$size]);
            if($oldid!==false)
            {
                $count_el=$ids[$oldid]['count'];
                unset($ids[$oldid]);
                $ids=array_merge($ids);//for reindex array
            }
            $this->cart->setToCart($ids, $this->cart->count-$count_el, $this->cart->price-$product->real_price*$count_el);

        }
        
        public function minusCart(Product $product, $size)
        {   
            $ids = $this->cart->ids;
            $count_el = 0;
            $oldid = $this->inids(['id'=>$product->id, 'size'=>$size]);
            if($oldid !== false)
            {
                $count_el = $ids[$oldid]['count'];
                if($count_el > 1)
                {
                    $ids[$oldid]['count']--;
                    $this->cart->setToCart($ids, $this->cart->count-1, $this->cart->price-$product->real_price);
                }
            }
        }    
        public function unsetCart()
        {
            $this->cart->unsetCart();
        }
        public function getCart()
        {
            return $this->cart->getCart();
        }
        private function inids($needle)
        {
            $haystack=$this->cart->ids;
            
            for($i=0; $i<count($haystack);$i++)
            {
                if($haystack[$i]['id']==$needle['id'] && $haystack[$i]['size']==$needle['size'])
                    return $i;
            }
            return false;
        }    
    }

?>