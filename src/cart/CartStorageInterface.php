<?php

namespace src\cart;

interface CartStorageInterface
{
    public function setToCart($ids, $count, $price);
    
    public function getCart();
    
    public function unsetCart();
}