<?php
namespace src\repositories\info;
use src\entities\info\Info;
use yii\web\NotFoundHttpException;

class InfoRepository
{
    public function get($id): Info
    {
        if (!$info = Info::findOne($id)) {
            throw new \DomainException('Страница не найдена');
        }
        return $info;
    }
    public function save(Info $info): Info
    {
        if (!$info->save()) {
            throw new \DomainException('Ошибка сохранения');
        }
        return $info;
    }
    public function remove(Info $info): void
    {
        if (!$info->delete()) {
            throw new \DomainException('Ошибка удаления');
        }
    }

    public function getPage($alias)
    {
        if (!$page = Info::findOne(['alias' => $alias])) {
            throw new NotFoundHttpException('Страница не найдена');
        }
        return $page;
    }

    public function getTopMenuPages()
    {
        if (!$pages = Info::find()->select(['name', 'alias', 'sort'])->where(['to_top' => 1])->orderBy('sort')->all()) {
            throw new NotFoundHttpException('Страниц не найдено');
        }
        return $pages;
    }

    public function getFooterMenuPages()
    {
        if (!$pages = Info::find()->select(['name', 'alias', 'sort'])->orderBy('sort')->all()) {
            throw new NotFoundHttpException('Страниц не найдено');
        }
        return $pages;
    }
}