<?php
namespace src\repositories\info;
use src\entities\info\Review;

class ReviewRepository
{
    public function get($id)
    {
        if (!$review = Review::findOne($id)) {
            throw new \DomainException('Отзыв не найден');
        }
        return $review;
    }
    public function save(Review $review)
    {
        if (!$review->save()) {
            throw new \DomainException('Ошибка сохранения');
        }
        return $review;
    }
    public function remove(Review $review)
    {
        if (!$review->delete()) {
            throw new \DomainException('Ошибка удаления');
        }
    }
    public function findAllActive()
    {
        return Review::find()->where(['active'=>Review::ACTIVE])->orderBy('id DESC')->all();
    }
}