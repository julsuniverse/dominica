<?php
namespace src\repositories;
use src\entities\Settings;
use yii\web\NotFoundHttpException;

class SettingsRepository
{
    public function get($id): Settings
    {
        if (!$settings = Settings::findOne($id)) {
            throw new \DomainException('Размер не найден');
        }
        return $settings;
    }

    public function save(Settings $settings): Settings
    {
        if (!$settings->save()) {
            throw new \DomainException('Ошибка сохранения');
        }
        return $settings;
    }

    public function remove(Settings $settings): void
    {
        if (!$settings->delete()) {
            throw new \DomainException('Ошибка удаления');
        }
    }

    public function getMain()
    {
        if(!$settings = Settings::find()->select(['main_h1', 'main_text', 'main_img', 'main_seo_title', 'main_seo_text', 'main_seo_keys'])->where(['id' => 1])->limit(1)->one())
        {
            throw new NotFoundHttpException('Ничего не найдено');
        }
        return $settings;
    }

    public function getLayout()
    {
        if(!$settings = Settings::find()->select(['phone1', 'phone2', 'contact_email', 'admin_email'])->where(['id' => 1])->limit(1)->one())
        {
            throw new NotFoundHttpException('Ничего не найдено');
        }
        return $settings;
    }

    public function getSale()
    {
        if(!$settings = Settings::find()->select([ 'sale_seo_title', 'sale_seo_text', 'sale_seo_keys'])->where(['id' => 1])->limit(1)->one())
        {
            throw new NotFoundHttpException('Ничего не найдено');
        }
        return $settings;
    }

    public function getArchive()
    {
        if(!$settings = Settings::find()->select([ 'archive_seo_title', 'archive_seo_text', 'archive_seo_keys'])->where(['id' => 1])->limit(1)->one())
        {
            throw new NotFoundHttpException('Ничего не найдено');
        }
        return $settings;
    }

    public function getAdminEmail()
    {
        if(!$email = Settings::find()->select(['admin_email'])->where(['id' => 1])->limit(1)->one()->admin_email)
        {
            throw new NotFoundHttpException('Ничего не найдено');
        }
        return $email;
    }

}