<?php
namespace src\repositories\shop;
use src\entities\shop\Section;
use yii\web\NotFoundHttpException;

class SectionRepository
{
    public function get($id): Section
    {
        if (!$section = Section::findOne($id)) {
            throw new \DomainException('Секция не найдена');
        }
        return $section;
    }
    public function save(Section $section): Section
    {
        if (!$section->save()) {
            throw new \DomainException('Ошибка сохранения');
        }
        return $section;
    }
    public function remove(Section $section): void
    {
        if (!$section->delete()) {
            throw new \DomainException('Ошибка удаления');
        }
    }
    public function getByAlias($alias): Section
    {
        if (!$section = Section::findOne(['alias' => $alias])) {
            throw new NotFoundHttpException('Раздел не найден');
        }
        return $section;
    }
    public function findAllSections()
    {
        return Section::find()->all();
    }
    public function findSectionsWithCategories()
    {
        return Section::find()
            ->joinWith([
                'categories' => function ($query) {
                    $query->orderBy('category.lft');
                },
            ])
            ->all();
    }
    public function findSectionsWithCategoriesFirstLvl()
    {
        return Section::find()
            ->joinWith([
                'categories' => function ($query) {
                    $query->where(['category.depth' => 1])->orderBy('category.lft');
                },
            ])
            ->all();
    }
    public function findSectionsColumn()
    {
        return Section::find()->select(['name', 'id'])->indexBy('id')->column();
    }
}