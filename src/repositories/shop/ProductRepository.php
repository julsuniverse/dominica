<?php
namespace src\repositories\shop;

use src\entities\shop\Product;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use src\helpers\SizeHelper;

class ProductRepository
{
    public function save(Product $product) : Product
    {
        if(!$product->save(false))
        {
            throw new \DomainException('Ошибка сохранения товара');
        }
        return $product;
    }

    public function get($id): Product
    {
        if (!$product = Product::findOne($id)) {
            throw new \DomainException('Товар не найден');
        }
        return $product;
    }
    
    public function getPhotos($id) : Product
    {
        if (!$product = Product::find()->select(['photos'])->where(['id' => $id])->one()) {
            throw new \DomainException('Товар не найден');
        }
        return $product;
    }

    public function getByAlias($alias): Product
    {
        if (!$product = Product::findOne(['alias' => $alias])) {
            throw new NotFoundHttpException('Товар не найден');
        }
        return $product;
    }

    public function remove(Product $product): void
    {
        if (!$product->delete()) {
            throw new \DomainException('Ошибка удаления');
        }
    }
    public function findForCatalog($section, $category, $sizes, $sort)
    {
        $query = Product::find()->where(['>', 'amount', 0]);
        $query->joinWith('category');
        
        if($section)
            $query->andWhere(['product.section_id'=>$section]);
        if($category)
            $query->andWhere(['or', ['category_id'=>$category], ['subcategory_id'=>$category]]);
            
        if(!$sort || $sort=="date")
            $query->orderBy("id");
        elseif($sort=="name")
            $query->orderBy("name");
        elseif($sort=="price")
            $query->orderBy("price");
            
        if($sizes)
        {
            $exp = SizeHelper::getRegExp($sizes);
            $query->andWhere(['REGEXP', 'sizes', $exp]);
        }
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6,
                'pageSizeParam' => false
            ],
        ]);
            
        return $provider;
    }
    public function findForArchive($section, $category, $sizes, $sort)
    {
        $query = Product::find()->where(['or', ['amount'=> 0], ['archive' => 1]]);
        $query->joinWith('category');
        
        if($section)
            $query->andWhere(['product.section_id'=>$section]);
        if($category)
            $query->andWhere(['or', ['category_id'=>$category], ['subcategory_id'=>$category]]);
            
        if(!$sort || $sort=="date")
            $query->orderBy("id");
        elseif($sort=="name")
            $query->orderBy("name");
        elseif($sort=="price")
            $query->orderBy("price");
            
        if($sizes)
        {
            $exp = SizeHelper::getRegExp($sizes);
            $query->andWhere(['REGEXP', 'sizes', $exp]);
        }
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6,
                'pageSizeParam' => false
            ],
        ]);
            
        return $provider;
    }
    public function findSale($section)
    {
        $query = Product::find()->where(['>', 'amount', 0])->andWhere(['>', 'sale', 1]);
        $query->joinWith('category');
        
        if($section)
            $query->andWhere(['product.section_id'=>$section]);
            
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6,
                'pageSizeParam' => false
            ],
        ]);
            
        return $provider;
    }
    public function findRecomendet()
    {
        return Product::find()->joinWith('category')->where(['product.recommended'=>1])->orderBy('product.id DESC')->limit(8)->all();
    }
    public function findProdsToMainPage()
    {
        return [
            'home' => Product::find()->joinWith('category')->where(['product.home'=>1])->orderBy('product.id DESC')->limit(20)->all(),
            'rest' => Product::find()->joinWith('category')->where(['product.rest'=>1])->orderBy('product.id DESC')->limit(20)->all(),
            'casual' => Product::find()->joinWith('category')->where(['product.casual'=>1])->orderBy('product.id DESC')->limit(20)->all(),
        ];
    }

 }