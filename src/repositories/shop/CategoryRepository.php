<?php
namespace src\repositories\shop;

use src\entities\shop\Category;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class CategoryRepository
{
    public function get($id): Category
    {
        if (!$category = Category::findOne($id)) {
            throw new \DomainException('Категория не найдена');
        }
        return $category;
    }
    public function getByAlias($section_id, $alias): Category
    {
        if (!$category = Category::findOne(['section_id'=>$section_id, 'alias'=>$alias])) {
            throw new NotFoundHttpException('Категория не найдена');
        }
        return $category;
    }
    public function save(Category $category): Category
    {
        if($this->ifExists($category))
        {
            throw new \DomainException('Такая категория уже есть');
        }
        if (!$category->save()) {
            throw new \DomainException('Ошибка сохранения');
        }
        return $category;
    }
    public function remove(Category $category): void
    {
        if (!$category->delete()) {
            throw new \DomainException('Ошибка удаления');
        }
    }
    public function ifExists($category)
    {
        $query = Category::find()
            ->where(['alias'=>$category->alias, 'section_id'=>$category->section_id, 'parent_id'=>$category->parent_id]);
        if($category->id)
            $query->andWhere(['!=', 'id', $category->id]);
            
        return $query->exists();
    }
    public function findParentsColumn($section_id)
    {            
        return ArrayHelper::map(
            Category::find()->where(['id' => 1])->orWhere(['depth' =>  1, 'section_id' => $section_id])->orderBy('lft')->asArray()->all(), 
            'id', 
            function (array $category) {
                return ($category['depth'] > 1 ? str_repeat('-- ', $category['depth'] - 1) . ' ' : '') . $category['name'];
         });
    }

    public function findCategoryInSection($section_id)
    {
        return Category::find()->select(['name', 'id'])->where(['depth' =>  1, 'section_id' => $section_id])->orderBy('lft')->indexBy('id')->column();
    }

    public function findSubCategoryInSection($category_id)
    {
        return Category::find()->select(['name', 'id'])->where(['parent_id' => $category_id])->orderBy('lft')->indexBy('id')->column();
    }

    public function findCategories()
    {
        return Category::find()->orderBy('lft')->all();
    }
}