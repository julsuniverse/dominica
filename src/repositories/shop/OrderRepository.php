<?php
namespace src\repositories\shop;
use src\entities\shop\Order;

class OrderRepository
{
    public function get($id): Order
    {
        if (!$order = Order::findOne($id)) {
            throw new \DomainException('Заказ не найден');
        }
        return $order;
    }
    public function save(Order $order): Order
    {
        if (!$order->save()) {
            throw new \DomainException('Ошибка сохранения заказа');
        }
        return $order;
    }
    public function remove(Order $order): void
    {
        if (!$order->delete()) {
            throw new \DomainException('Ошибка удаления заказа');
        }
    }

    public function getOrders($ids)
    {
        if(!$order = Order::find()->where(['id' => $ids])->all())
            throw new \DomainException('Заказы не найдены');
        return $order;
    }
}