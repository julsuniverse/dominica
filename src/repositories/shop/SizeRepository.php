<?php
namespace src\repositories\shop;
use src\entities\shop\Size;
use yii\web\NotFoundHttpException;

class SizeRepository
{
    public function get($id): Size
    {
        if (!$size = Size::findOne($id)) {
            throw new \DomainException('Размер не найден');
        }
        return $size;
    }

    public function save(Size $size): Size
    {
        if (!$size->save()) {
            throw new \DomainException('Ошибка сохранения');
        }
        return $size;
    }

    public function remove(Size $size): void
    {
        if (!$size->delete()) {
            throw new \DomainException('Ошибка удаления');
        }
    }
    public function findAll()
    {
        return Size::find()->all();
    }

    public function getSizes()
    {
        return Size::find()->select(['name', 'alias'])->indexBy('alias')->column();
    }
}