<?php
namespace src\repositories\shop;
use src\entities\shop\Orderitem;

class OrderitemRepository
{
    public function save(Orderitem $orderitem): Orderitem
    {
        if (!$orderitem->save(false)) {
            throw new \DomainException('Ошибка сохранения структуры заказа');
        }
        return $orderitem;
    }
    public function remove(Orderitem $orderitem): void
    {
        if (!$orderitem->delete()) {
            throw new \DomainException('Ошибка удаления');
        }
    }
    public function findWithProducts($id)
    {
        return Orderitem::find()
            ->joinWith('product')
            ->where(['order_id'=>$id])
            ->all();
    }
}