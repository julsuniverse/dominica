<?php

namespace common\bootstrap;

use src\services\contact\ContactService;
use src\services\order\OrderService;
use yii\base\BootstrapInterface;
use yii\mail\MailerInterface;
use src\forms\shop\CategoryForm;
use src\repositories\shop\CategoryRepository;
use src\repositories\shop\SectionRepository;
use src\cart\Cart;
use src\cart\CartSessionStorage;


class SetUp implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $container = \Yii::$container;

        $container->setSingleton(MailerInterface::class, function() use ($app)
        {
            return $app->mailer;
        });

        $container->setSingleton(ContactService::class, [], [
            $app->params['adminEmail'],
        ]);

        
        $container->setSingleton(Cart::class, function () use ($app) {
            return new Cart(
                new CartSessionStorage($app->session)
            );
        });
    }
}