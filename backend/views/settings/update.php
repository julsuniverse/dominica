<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model src\entities\Settings */

$this->title = 'Редактировать настройки';
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="settings-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'settings' => $settings,
    ]) ?>

</div>
