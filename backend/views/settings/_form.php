<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;


/* @var $this yii\web\View */
/* @var $model src\entities\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'main_h1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_text')->textarea(['rows' => 6]) ?>

    <?php if($settings->main_img){echo "<img style='width:100px;' src='../../frontend/web/img/".$settings->main_img."'/>";}?>
    <?= $form->field($model, 'main_img')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'placeholder' => 'Выберите файл'],
        'pluginOptions' => ['previewFileType' => 'image','showUpload' => false]
    ]); ?>

    <?= $form->field($model, 'main_seo_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_seo_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'main_seo_keys')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'phone1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admin_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sale_seo_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sale_seo_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sale_seo_keys')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'archive_seo_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'archive_seo_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'archive_seo_keys')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>    </div>

    <?php ActiveForm::end(); ?>

</div>
