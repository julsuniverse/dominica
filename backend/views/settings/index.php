<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Settings', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'main_h1',
            'main_text',
            'main_img',
            'main_seo_title',
            // 'main_seo_text',
            // 'main_seo_keys',
            // 'phone1',
            // 'phone2',
            // 'contact_email:email',
            // 'admin_email:email',
            // 'sale_seo_title',
            // 'sale_seo_text',
            // 'sale_seo_keys',
            // 'archive_seo_title',
            // 'archive_seo_text',
            // 'archive_seo_keys',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
