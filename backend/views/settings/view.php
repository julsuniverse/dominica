<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model src\entities\Settings */

$this->title = "Настройки";
$this->params['breadcrumbs'][] = ['label' => 'Настройки', 'url' => ['view', 'id' => 1]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'main_h1',
            'main_text',
            [
                'attribute'=>'main_img',
                'value'=>'../../frontend/web/img/'.$model->main_img,
                'format' => ['image',['width'=>'100px']],
            ],
            'main_seo_title',
            'main_seo_text',
            'main_seo_keys',
            'phone1',
            'phone2',
            'contact_email:email',
            'admin_email:email',
            'sale_seo_title',
            'sale_seo_text',
            'sale_seo_keys',
            'archive_seo_title',
            'archive_seo_text',
            'archive_seo_keys',
        ],
    ]) ?>

</div>
