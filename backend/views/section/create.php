<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model src\entities\shop\Section */

$this->title = 'Создать секцию';
$this->params['breadcrumbs'][] = ['label' => 'Секции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
