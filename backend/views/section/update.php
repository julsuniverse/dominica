<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model src\entities\shop\Section */

$this->title = 'Редактировать секцию: ' . $section->name;
$this->params['breadcrumbs'][] = ['label' => 'Секции', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $section->name, 'url' => ['view', 'id' => $section->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="section-update">

    <?= $this->render('_form', [
        'model' => $model,
        'section' => $section
    ]) ?>

</div>
