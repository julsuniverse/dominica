<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Секции';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section-index">

    <p>
        <?= Html::a('Создать секцию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            //'id',
            'name',
            [
                'attribute' => 'photo',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(Url::toRoute('../../frontend/web/img/'.$data->photo),[
                        'style' => 'width:100px;'
                    ]);
                },
            ],
            [
                'attribute' => 'mini_photo',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(Url::toRoute('../../frontend/web/img/'.$data->mini_photo),[
                        'style' => 'width:100px;'
                    ]);
                },
            ],
            //'alias',
            'desc:ntext',
            //'seo_title',
            // 'seo_desc:ntext',
            // 'seo_keys:ntext',

        ],
    ]); ?>
</div>
