<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model src\entities\shop\Section */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Секции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            //'photo',
            [
                'attribute'=>'photo',
                'value'=>'../../frontend/web/img/'.$model->photo,
                'format' => ['image',['width'=>'100px']],
            ],
            [
                'attribute'=>'mini_photo',
                'value'=>'../../frontend/web/img/'.$model->mini_photo,
                'format' => ['image',['width'=>'100px']],
            ],
            'alias',
            'desc:ntext',
            'seo_title',
            'seo_desc:ntext',
            'seo_keys:ntext',
        ],
    ]) ?>

</div>
