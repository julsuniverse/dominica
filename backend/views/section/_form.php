<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model src\entities\shop\Section */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="section-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'desc')->textarea(['rows' => 6]) ?>
    
    <?php if($section->photo){echo "<img style='width:100px;' src='../../frontend/web/img/".$section->photo."'/>";}?>
    <?= $form->field($model, 'photo')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => ['previewFileType' => 'image','showUpload' => false]
    ]); ?>
    
    <?php if($section->mini_photo){echo "<img style='width:100px;' src='../../frontend/web/img/".$section->mini_photo."'/>";}?>
    <?= $form->field($model, 'mini_photo')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => ['previewFileType' => 'image','showUpload' => false]
    ]); ?>

    <?= $form->field($model->meta, 'seo_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model->meta, 'seo_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model->meta, 'seo_keys')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
