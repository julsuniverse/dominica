<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отзывы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete} {active}',
                'buttons' => [
                    'active' => function ($url,$model) {
                            return !$model->active ? Html::a(
                            ' <span class="glyphicon glyphicon-ok"></span> ', 
                            Url::toRoute(['/review/active', 'id'=>$model->id]), ['data-toggle'=>"tooltip", 'data-placement'=>"bottom", 'title'=>"Сделать активным"]) : null;
                    },
                ],
            ],
            //'id',
            'name',
            'stars',
            [
                'attribute' => 'active',
                'value' => function($data){
                    return $data->active ? "Активен" : "Неактивен";
                }
            ],
            [
                'attribute' => 'date',
                'value' => function($data){
                    return date('H:i d-m-Y', $data->date);
                }
            ],
            //'text:ntext',
        ],
    ]); ?>
</div>
