<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model src\entities\info\Review */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Отзіві', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-view">

    <p>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены, что хотите удалить отзыв?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'text:ntext',
            'stars',
            [
                'attribute' => 'date',
                'value' => date('H:i d-m-Y', $model->date)
            ],
            [
                'attribute' => 'active',
                'value' => $model->active ? "Активен" : "Неактивен"
            ],
        ],
    ]) ?>

</div>
