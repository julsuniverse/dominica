<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model src\entities\info\Info */

$this->title = 'Создать страницу';
$this->params['breadcrumbs'][] = ['label' => 'Страницы информации', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
