<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model src\entities\info\Info */

$this->title = 'Редактировать страницу: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Страницы информации', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="info-update">

    <?= $this->render('_form', [
        'model' => $model,
        'info' => $info
    ]) ?>

</div>
