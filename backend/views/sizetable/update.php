<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model src\entities\shop\Sizetable */

$this->title = 'Редактировать размер: ' . $model->size_number;
$this->params['breadcrumbs'][] = ['label' => 'Таблица размеров', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="sizetable-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
