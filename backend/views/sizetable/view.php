<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model src\entities\shop\Sizetable */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sizetables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sizetable-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'gender',
            'chest_start',
            'chest_end',
            'waist_start',
            'waist_end',
            'thigh_start',
            'thigh_end',
            'size_number',
            'size_letter',
        ],
    ]) ?>

</div>
