<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model src\entities\shop\Sizetable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sizetable-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'gender')->widget(Select2::className(), [
        'data' => ['w' => 'Женщина', 'm' => 'Мужчина'],
        'size' => Select2::MEDIUM,
        'options' => ['placeholder'=>'Выберите пол', 'multiple' => false],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])   ?>

    <?= $form->field($model, 'chest_start')->textInput() ?>

    <?= $form->field($model, 'chest_end')->textInput() ?>

    <?= $form->field($model, 'waist_start')->textInput() ?>

    <?= $form->field($model, 'waist_end')->textInput() ?>

    <?= $form->field($model, 'thigh_start')->textInput() ?>

    <?= $form->field($model, 'thigh_end')->textInput() ?>

    <?= $form->field($model, 'size_number')->textInput() ?>

    <?= $form->field($model, 'size_letter')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
