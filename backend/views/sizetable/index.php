<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Таблица размеров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sizetable-index">

<?php Pjax::begin(['id' => 'sizetable_woman']); ?>
<h2>Для женщин</h2>
<?= GridView::widget([
        'dataProvider' => $dataProviderW,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],

            //'id',
            //'gender',
            'size_number',
            'size_letter',
            'chest_start',
            'chest_end',
            'waist_start',
            'waist_end',
            'thigh_start',
            'thigh_end',


        ],
    ]); ?>
<?php Pjax::end(); ?>

<?php Pjax::begin(['id' => 'sizetable_man']); ?>
<h2>Для мужчин</h2>
<?= GridView::widget([
    'dataProvider' => $dataProviderM,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],

        //'id',
        //'gender',
        'size_number',
        'size_letter',
        'chest_start',
        'chest_end',
        'waist_start',
        'waist_end',
        //'thigh_start',
        //'thigh_end',

    ],
]); ?>
<?php Pjax::end(); ?>

</div>
