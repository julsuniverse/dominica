<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model src\entities\shop\Sizetable */

$this->title = 'Create Sizetable';
$this->params['breadcrumbs'][] = ['label' => 'Sizetables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sizetable-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
