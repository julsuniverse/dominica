<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use src\entities\shop\Order;
/* @var $this yii\web\View */
/* @var $model src\entities\shop\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->dropDownList(Order::getStatusArray()) ?>

    <?= $form->field($model, 'total')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'poshta')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'post_number')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'pay_type')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'additional')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
