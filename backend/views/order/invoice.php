<?php

use src\entities\shop\Order;
use yii\helpers\Html;
use yii\widgets\DetailView;
?>
<div class="order-view" style="background-color: #fff !important;">
    <?php foreach($orders as $order){?>
        <div style="border: 1px solid #000; margin-bottom: 60px;">
            <?= DetailView::widget([
                'model' => $order,
                'attributes' => [
                    'id',
                    [
                        'filter'=>Order::getStatusArray(),
                        'attribute'=>'status',
                        'format' => 'html',
                        'value' => function($data){
                            return Order::getStatusName($data->status);
                        }
                    ],
                    'total',
                    [
                        'attribute'=>'date',
                        'filter' => false,
                        'value'=> function($data){
                            return date('d.m.Y H:i', $data->date);
                        }
                    ],
                    'name',
                    'phone',
                    'city',
                    'poshta:ntext',
                    'post_number',
                ],
            ]) ?>
            <h6>Структура заказа</h6>
            <?=$this->render('_struct', ['orderstructs'=> $order->orderitems]);?>
        </div>
    <?php }?>
</div>
