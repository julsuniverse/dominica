<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use src\entities\shop\Order;

/* @var $this yii\web\View */
/* @var $model src\entities\shop\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить зака?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute'=>'status',
                'value'=>Order::getStatusName($model->status)
            ],
            'total',
            [
                'attribute'=>'date',
                'value'=> date('d.m.Y H:i:s', $model->date)
            ],
            'name',
            'phone',
            'email:email',
            'city',
            'poshta:ntext',
            'pay_type:ntext',
            'post_number',
            'additional:ntext',
        ],
    ]) ?>
    <h4>Структура заказа</h4>
    <?=$this->render('_struct', ['orderstructs'=>$orderstructs]);?>

</div>
