<div class="table-responsive">
  <table class="table">
    <tr>
        <th>Фото</th>
        <th>Товар</th>
        <th>Код</th>
        <th>Количество</th>
        <th>Размер</th>
        <th>Цена за единицу</th>
    </tr>
    <?php foreach($orderstructs as $str){?>
    <tr>
        <td class="imgorder"><img style="max-width: 80px;" src="../../frontend/web/img/<?=$str->product->photo;?>" /></td>
        <td><?=$str->product->name;?></td>
        <td><?=$str->product->code;?></td>
        <td><?=$str->amount;?></td>
        <td><?=$str->size;?></td>
        <td><?=$str->price.' грн';?></td>
    </tr>
    <?php }?>
  </table>
</div>