<?php

use yii\helpers\Html;
use yii\grid\GridView;
use src\entities\shop\Order;

/* @var $this yii\web\View */
/* @var $searchModel backend\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <p>
        <?= Html::a('Печать', [''], ['class' => 'btn btn-success invoice_btn']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=>function($model){
                if($model->status==Order::STATUS_NEW){
                    return ['class' => 'info'];
                }
                if($model->status==Order::STATUS_DISPATCH){
                    return ['class' => 'warning'];
                }
                if($model->status==Order::STATUS_SUCCESS){
                    return ['class' => 'success'];
                }
                if($model->status==Order::STATUS_CANCELED){
                    return ['class' => 'danger'];
                }
        },
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'cssClass' => 'invoice_check',
                // чекбоксы
            ],
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            'id',
            [
                'filter'=>Order::getStatusArray(),
                'attribute'=>'status',
                'format' => 'html',
                'value' => function($data){
                    return Order::getStatusName($data->status);
                }
            ],
            'total',
            [
                'label'=>'Структура',
                'format'=>'html',
                'content' => function($data){
                    return '<a href="#" data-id="'.$data->id.'" class="showstruct">структура</a>';
                }
            ],
            [
                'attribute'=>'date',
                'filter' => false,
                'value'=> function($data){
                   return date('d.m.Y H:i', $data->date);
                }
            ],
            'name',
            'phone',
            // 'email:email',
            // 'city',
            // 'poshta:ntext',
            // 'pay_type:ntext',
            // 'additional:ntext',
        ],
    ]); ?>
</div>
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModal1Label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModal1Label">Структура заказа</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
    </div>
  </div>
</div>
<?php
$js = <<< JS
$('.order-index').on('click', '.invoice_btn', function(e){
    e.preventDefault();
    var keys = $('.grid-view').yiiGridView('getSelectedRows');
    console.log(keys);
    location.href='/yii-admin/order/invoice?ids='+keys;
});
JS;
$this->registerJs($js);
?>