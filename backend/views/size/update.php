<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model src\entities\shop\Size */

$this->title = 'Редактировать размер: ' . $size->name;
$this->params['breadcrumbs'][] = ['label' => 'Размеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $size->name, 'url' => ['view', 'id' => $size->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="size-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
