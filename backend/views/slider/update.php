<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model src\entities\Slider */

$this->title = 'Редактировать слайд: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="slider-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
