<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слайдер на главной';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">

    <p>
        <?= Html::a('Добавить слайд', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            [
                'attribute' => 'photo',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(Url::toRoute('../../frontend/web/img/'.$data->photo),[
                        'style' => 'width:100px;'
                    ]);
                },
            ],
            'href',
            'position',
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
