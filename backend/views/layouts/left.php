<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Aдминистратор</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Заказы', 'icon' => 'file-code-o', 'url' => ['/order'], 'active' => $this->context->id == 'order'],
                    ['label' => 'Меню управления', 'options' => ['class' => 'header']],
                    ['label' => 'Секции', 'icon' => 'file-code-o', 'url' => ['/section'], 'active' => $this->context->id == 'section'],
                    ['label' => 'Категории', 'icon' => 'file-code-o', 'url' => ['/category'], 'active' => $this->context->id == 'category'],
                    ['label' => 'Размеры', 'icon' => 'file-code-o', 'url' => ['/size'], 'active' => $this->context->id == 'size'],
                    ['label' => 'Товар', 'icon' => 'file-code-o', 'url' => ['/product'], 'active' => $this->context->id == 'product'],
                    ['label' => 'Страницы информации', 'icon' => 'file-code-o', 'url' => ['/info'], 'active' => $this->context->id == 'info'],
                    ['label' => 'Отзывы', 'icon' => 'file-code-o', 'url' => ['/review'], 'active' => $this->context->id == 'review'],
                    ['label' => 'Настройки', 'icon' => 'file-code-o', 'url' => ['/settings/view?id=1'], 'active' => $this->context->id == 'settings'],
                    ['label' => 'Слайдер', 'icon' => 'file-code-o', 'url' => ['/slider'], 'active' => $this->context->id == 'slider'],
                    ['label' => 'Таблица размеров', 'icon' => 'file-code-o', 'url' => ['/sizetable'], 'active' => $this->context->id == 'sizetable'],

                ],
            ]
        ) ?>

    </section>

</aside>
