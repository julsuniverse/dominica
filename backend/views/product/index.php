<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать товар', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            'name',
            'code',
            [
                'attribute' => 'section_id',
                'filter' => $searchModel->sectionList(),
                'value' => 'section.name',
            ],
            [
                'attribute' => 'category_id',
                'filter' => $searchModel->categoryList(),
                'value' => 'category.name',
            ],
            [
                'attribute' => 'subcategory_id',
                'filter' => $searchModel->subcategoryList(),
                'value' => 'subcategory.name',
            ],
            [
                'attribute' => 'photo',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(Url::toRoute('../../frontend/web/img/'.$data->photo),[
                        'style' => 'width:100px;'
                    ]);
                },
            ],
            'price',
            [
                'attribute' => 'sale',
                'value' => function ($data) {
                    if(!$data->sale || $data->sale == 0)
                        return "-";
                    else
                        return Yii::$app->formatter->asPercent($data->sale/100, 0);
                }
            ],

        ],
    ]); ?>
<?php Pjax::end(); ?></div>
