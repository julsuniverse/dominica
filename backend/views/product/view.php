<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model src\entities\shop\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
        $photos=explode("##", $model->photos);
        $str_photos="";
        foreach ($photos as $photo)
        {
            if($photo)
                $str_photos.="<img style='width:100px;' src='/frontend/web/img/$photo'/>";
        }

        /*$size = Json::decode($model->sizes);
        foreach($size as $s)
            $size_str .= $s."; ";; */

    ?>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'code',
            [
                'attribute' => 'section_id',
                'value' => $model->section->name,
            ],
            [
                'attribute' => 'category_id',
                'value' => $model->category->name,
            ],
            [
                'attribute' => 'subcategory_id',
                'value' => $model->subcategory->name,
            ],
            [
                'attribute'=>'photo',
                'value'=>'../../frontend/web/img/'.$model->photo,
                'format' => ['image',['width'=>'100px']],
            ],
            [
                'attribute'=>'photos',
                'value'=>$str_photos,
                'format' => 'html',
            ],
            'polotno',
            'price',
            [
                'attribute' => 'sale',
                'value' => Yii::$app->formatter->asPercent($model->sale/100, 0),
            ],
            [
                'attribute' => 'sizes',
                //'value' => $size_str,
                'value' => implode(', ', $model->sizes),
            ],
            //'sizes',
            'short_desc',
            'desc:ntext',
            'in_stock:boolean',
            'amount',
            'archive:boolean',
            'recommended:boolean',
            'home:boolean',
            'rest:boolean',
            'casual:boolean',
            'seo_title',
            'seo_desc:ntext',
            'seo_keys:ntext',
        ],
    ]) ?>

</div>
