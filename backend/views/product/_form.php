<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model src\entities\shop\Product */
/* @var $form yii\widgets\ActiveForm */
//print_r (Json::decode($product->sizes));
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'section_id')->dropDownList($model->findSectionsColumn(), ['prompt'=>'']) ?>

    <?= $form->field($model, 'category_id')->dropDownList($model->findParentsColumn(), ['prompt'=>'']) ?>

    <?= $form->field($model, 'subcategory_id')->dropDownList($model->findSubColumn(), ['prompt'=>'']) ?>

    <?php if($product->photo){echo "<img style='width:100px;' src='../../frontend/web/img/".$product->photo."'/>";}?>
    <?= $form->field($model, 'photo')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'placeholder' => 'Выберите файл'],
        'pluginOptions' => ['previewFileType' => 'image','showUpload' => false]
    ]); ?>

    <?php if($product->photos){
        $photos=explode("##", $product->photos);
        foreach ($photos as $photo){?>
            <?php if($photo){ ?>
                <div class="ph">
                    <img style='width:100px;' src='../../frontend/web/img/<?=$photo;?>'/>
                    <a href="<?=Url::toRoute(['product/deletephoto', 'product_id'=>$product->id, 'name'=>trim($photo)]);?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                </div>
            <?php }?>
        <?php } }?>
    <div class="clear"></div>

    <?= $form->field($model, 'photos[]')->widget(FileInput::classname(), [
        'options' => ['multiple' => true, 'accept' => 'image/*'],
        'pluginOptions' => ['previewFileType' => 'image','showUpload' => false]
    ]);?>

    <?= $form->field($model, 'polotno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'sale')->textInput() ?>
    <?= $form->field($model, 'sizes')->widget(Select2::className(), [
        'data' => $model->getSizes(),
        'size' => Select2::MEDIUM,
        'options' => ['placeholder'=>'Выберите размеры', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])   ?>

    <?= $form->field($model, 'short_desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'desc')->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions(['elfinder'],[]),
    ]); ?>

    <?= $form->field($model, 'in_stock')->checkbox(); ?>
    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'archive')->checkbox(); ?>

    <?php//= $form->field($model, 'recommended')->checkbox(); ?>
    
    <?= $form->field($model, 'home')->checkbox(); ?>
    <?= $form->field($model, 'rest')->checkbox(); ?>
    <?= $form->field($model, 'casual')->checkbox(); ?>

    <?= $form->field($model->meta, 'seo_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model->meta, 'seo_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model->meta, 'seo_keys')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
