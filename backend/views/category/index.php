<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">
    <p>
        <?= Html::a('Создать категорию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(['id'=>'category_tree']);?>
    <?php foreach($sections as $section){?>
    <div class="col-md-5">
        <h3 class="text-success"><?=$section->name;?></h3>
        <?php foreach($section->categories as $cat){?>
            <?php //if($cat->section_id==$section->id){?>
                <h4><?=str_repeat('---- ', $cat->depth - 1) . $cat->name;?> 
                    <a href="<?=Url::toRoute(['category/update', 'id'=>$cat->id]);?>">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a> 
                    <?= Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', ['move-up', 'id' => $cat->id]);?>
                    <?= Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', ['move-down', 'id' => $cat->id]);?>
                    <?= Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>', ['delete', 'id' => $cat->id], [ 'data' => ['confirm' => 'Вы уверены, что желаете удалить категорию?','method' => 'post', ], ]) ?>
                </h4>
            <?php //}?>
        <?php }?>    
    </div>
    <?php }?>
    <?php Pjax::end();?>
    
    <?php /*= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'alias',
            'section_id',
            'parent_id',
            // 'desc:ntext',
            // 'seo_title',
            // 'seo_desc:ntext',
            // 'seo_keys:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); */?>
</div>
<style>
    h4
    {
        margin-left: 25px;
    }
    h5
    {
        margin-left: 45px;
    }
    span
    {
        font-size: 14px;
        margin-left: 7px;
    }
</style>
