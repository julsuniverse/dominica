<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model src\entities\shop\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'section_id')->dropDownList($model->findSectionsColumn(), ['prompt'=>'']) ?>

    <?= $form->field($model, 'parent_id')->dropDownList( $model->section_id ? $model->findParentsColumn() : []) ?>

    <?= $form->field($model, 'desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model->meta, 'seo_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model->meta, 'seo_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model->meta, 'seo_keys')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' =>'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
