<?php

namespace backend\controllers;

use Yii;
use src\entities\shop\Category;
use src\forms\shop\CategoryForm;
use src\services\shop\CategoryService;
use src\repositories\shop\CategoryRepository;
use src\repositories\shop\SectionRepository;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    private $service;
    private $categories;
    private $sections;

    public function __construct(
        $id, 
        $module, 
        CategoryService $service, 
        CategoryRepository $categories, 
        SectionRepository $sections,
        array $config = []
    )
    {
        $this->service = $service;
        $this->categories = $categories;
        $this->sections = $sections;
        parent::__construct($id, $module, $config);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index', [
            //'categories' => $this->categories->findCategories(),
            'sections' => $this->sections->findSectionsWithCategories()
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     *
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }*/

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new CategoryForm(null, $this->categories, $this->sections);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try
            {
                $category = $this->service->create($form);
                return $this->redirect(['index']);
            }
            catch(\DomainException $e)
            {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        
        return $this->render('create', [
            'model' => $form,
        ]);
        
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $category = $this->findModel($id);
        
        $form = new CategoryForm($category, $this->categories, $this->sections);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) 
        {
           try
           {
                $this->service->edit($form, $category->id);
                return $this->redirect(['index']);
           }
            catch(\DomainException $e)
            {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $form,
            'category' => $category
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }
    /**
     * @param integer $id
     * @return mixed
     */
    public function actionMoveUp($id)
    {
        $this->service->moveUp($id);
        return $this->redirect(['index']);
    }
    /**
     * @param integer $id
     * @return mixed
     */
    public function actionMoveDown($id)
    {
        $this->service->moveDown($id);
        return $this->redirect(['index']);
    }
    public function actionGetCatsFromSection($section)
    {
        $cats = $this->categories->findParentsColumn($section);
        echo Json::encode($cats);
    }

    public function actionGetCategoryInSectionProd($section_id)
    {
        $category = $this->categories->findCategoryInSection($section_id);
        echo Json::encode($category);
    }

    public function actionGetSubCategoryInSectionProd($category_id)
    {
        $category = $this->categories->findSubCategoryInSection($category_id);
        echo Json::encode($category);
    }
    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
