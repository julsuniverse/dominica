<?php

namespace backend\controllers;

use src\repositories\shop\OrderRepository;
use Yii;
use src\entities\shop\Order;
use src\forms\shop\OrderEditForm;
use src\services\shop\OrderService;
use backend\search\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use src\repositories\shop\OrderitemRepository;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    private $orderitems;
    private $service;
    private $orders;

    public function __construct(
        $id, 
        $module, 
        OrderitemRepository $orderitems, 
        OrderService $service,
        OrderRepository $orders,
        array $config = []
    )
    {
        $this->orderitems = $orderitems;
        $this->service = $service;
        $this->orders = $orders;
        parent::__construct($id, $module, $config);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'orderstructs'=>$this->orderitems->findWithProducts($id)
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $order = $this->findModel($id);
        
        $form = new OrderEditForm($order);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) 
        {
           try
           {
                $this->service->edit($form, $order->id);
                return $this->redirect(['index']);
           }
            catch(\DomainException $e)
            {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $form,
            'order' => $order
        ]);
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }
    
    public function actionGetstruct($id)
    {
        return $this->renderAjax('_struct', [
            'orderstructs'=>$this->orderitems->findWithProducts($id)
        ]);
    }

    public function actionInvoice($ids)
    {
        Yii::$app->response->format = 'pdf';
        $orders  = $this->orders->getOrders(explode(',', $ids));
        return $this->render('invoice', [
            'orders' => $orders,
        ]);
    }


    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
