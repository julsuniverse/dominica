<?php

namespace backend\controllers;

use src\forms\shop\ProductForm;
use src\repositories\shop\CategoryRepository;
use src\repositories\shop\SectionRepository;
use src\repositories\shop\SizeRepository;
use src\services\shop\ProductService;
use Yii;
use src\entities\shop\Product;
use backend\search\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{

    private $service;
    private $sections;
    private $categories;
    private $sizes;

    public function __construct(
        $id,
        $module,
        ProductService $service,
        SectionRepository $sections,
        CategoryRepository $categories,
        SizeRepository $sizes,
        array $config = []
    )
    {
        $this->service = $service;
        $this->sections = $sections;
        $this->categories = $categories;
        $this->sizes = $sizes;
        parent::__construct($id, $module, $config);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new ProductForm(null, $this->sections, $this->categories, $this->sizes);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try
            {
                $product = $this->service->create($form);
                return $this->redirect(['view', 'id' => $product->id]);
            }
            catch(\DomainException $e)
            {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);

    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $product = $this->findModel($id);

        $form = new ProductForm($product, $this->sections, $this->categories, $this->sizes);
        if ($form->load(Yii::$app->request->post()) && $form->validate())
        {
            try
            {
                $product = $this->service->edit($form, $product->id);
                return $this->redirect(['view', 'id' => $product->id]);
            }
            catch(\DomainException $e)
            {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $form,
            'product' => $product
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeletephoto($product_id, $name)
    {
        $this->service->deletePhoto($name, $product_id);
        //$product = Product::find()->where(['id' => $product_id])->limit(1)->one();
        //$product = Product::findOne($product_id);
        //print_r($product);
        //$product->photos = str_replace( $name.'##', '', $product->photos);
        //$product->sizes = "";
        //$product->save(false);
        return $this->redirect(['update', 'id' => $product_id]);
    }

}
