function getCategories(section, selector)
{
    var section = section.val();
    $.get('/yii-admin/category/get-cats-from-section', {section : section}, function(data){
        var new_options = '';//'<option value> </option>';
        var data= $.parseJSON(data);
        $.each(data, function(key, value) {
            new_options += '<option value="' + key + '">'+value+'</option>';
        });
        selector.html(new_options);
    });
}

function getCategoriesProd(section, selector)
{
    var section = section.val();
    $.get('/yii-admin/category/get-category-in-section-prod', {section_id : section}, function(data){
        var new_options = '<option value> </option>';
        var data= $.parseJSON(data);
        $.each(data, function(key, value) {
            new_options += '<option value="' + key + '">'+value+'</option>';
        });
        selector.html(new_options);
    });
}

function getSubCategoriesProd(category, selector)
{
    var category = category.val();
    $.get('/yii-admin/category/get-sub-category-in-section-prod', {category_id : category}, function(data){
        var new_options = '<option value> </option>';
        var data= $.parseJSON(data);
        $.each(data, function(key, value) {
            new_options += '<option value="' + key + '">'+value+'</option>';
        });
        selector.html(new_options);
    });
}


$(document).on('click', '.showstruct', function(e){
   e.preventDefault();
   var id = $(this).attr('data-id');
   $.get('/yii-admin/order/getstruct', {id : id}, function(data){
        $('#myModal1 .modal-body').html(data);
   });
   $('#myModal1').modal('show');
});

$('#categoryform-section_id').change(function(){
    getCategories($(this), $('#categoryform-parent_id'));
});

$('#productform-section_id').change(function(){
    getCategoriesProd($(this), $('#productform-category_id'));
});

$('#productform-category_id').change(function(){
    getSubCategoriesProd($(this), $('#productform-subcategory_id'));
});