<?php

namespace backend\search;

use src\entities\shop\Category;
use src\entities\shop\Section;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use src\entities\shop\Product;

/**
 * ProductSearch represents the model behind the search form about `src\entities\shop\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'section_id', 'category_id', 'subcategory_id', 'sale', 'amount'], 'integer'],
            [['name', 'code', 'alias', 'photo', 'photos', 'polotno', 'sizes', 'short_desc', 'desc', 'seo_title', 'seo_desc', 'seo_keys'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'section_id' => $this->section_id,
            'category_id' => $this->category_id,
            'subcategory_id' => $this->subcategory_id,
            'price' => $this->price,
            'sale' => $this->sale,
            'amount' => $this->amount,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'polotno', $this->polotno])
            ->andFilterWhere(['like', 'sizes', $this->sizes])
            ->andFilterWhere(['like', 'short_desc', $this->short_desc])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'seo_title', $this->seo_title])
            ->andFilterWhere(['like', 'seo_desc', $this->seo_desc])
            ->andFilterWhere(['like', 'seo_keys', $this->seo_keys]);

        return $dataProvider;
    }

    public function sectionList()
    {
        return Section::find()->select(['name', 'id'])->indexBy('id')->column();
    }

    public function categoryList()
    {
        return Category::find()->select(['name', 'id'])->indexBy('id')->column();
    }

    public function subcategoryList()
    {
        return Category::find()->select(['name', 'id'])->where(['depth' => 2])->indexBy('id')->column();
    }
}
